#!/bin/bash

CXX=g++
CXXFLAGS= -O9 -std=c++11 -Wall
DIRSRC= ./src/
EXEC=scores
ALL_QUAL=$(DIRSRC)Quality/density.o
LIB_PATH=/home/gaumont/CodingPlace/SocioPattern/src/linkstream/lib
all: $(EXEC)

scores : $(DIRSRC)main_evolution.o
	$(CXX) -L$(LIB_PATH) -Wl,-rpath,$(LIB_PATH) -lLinkstream -o $@ $^ $(CXXFLAGS)

##########################################
# Generic rules
##########################################

%.o: %.cpp %.h
	$(CXX) -o  $@ -c $< $(CXXFLAGS)


%.o: %.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS)

clean:
	rm -f $(DIRSRC)*.o

mrproper: clean
	rm -f *~ $(EXEC)
