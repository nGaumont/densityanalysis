Density study
=============

This program is the implementation of the scores defines in
"Finding remarkably dense sequences of contacts in link streams" from Gaumont et al.



## Compilation

This program is in C++11 and use cmake (>= 2.8.12).  
Make sure you have the approate version.  
To compile the program:

```sh
cd build/
make scores
```


## Usage

Here is a typical usage :  

```sh
./scores -i LINKSTREAM -c CANDIDATES
```

This computes the scores (start time, duration and node set) of all the candidates that in the file CANDIDATES.  
Is creates a file res_${g_id} for each group containing the group's characteristics.    

### Input files format

#### Linkstream

The Linkstream file should contain a list of undirected unweighted links.
A links should be in the format: `b,e,u,v`,  
where b and e are respectively the start and end time of the link between u and v.  
For example:  
```
1 2 4 5
1 3 1 2
2 6 2 4

```
The entry could be space or tab separated.

#### Candidates

The candidate file should contain a list of link assignment.   
Each line is composed of a link id and a group id.  
The link id is line where the link is in the file Linkstream minus 1.    
Thus the link id ranges from 0 to |E|-1.  
Each link should be in at most one group.


## Author

* Author : Gaumont Noé
* Email    : noe.gaumont@lip6.fr
* Location : Paris, France


## Disclaimer

This has been used in a Linux environment.  
Other operating systems should work.   
If you find a bug, please send a bug report to `noe.gaumont@lip6.fr`
including if necessary the input file and the parameters that caused the bug.  
You can also send me any comment or suggestion about the program.

## License