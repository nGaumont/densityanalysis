#include <iostream>
#include <time.h>
#include <limits>
#include <iomanip>
#include <exception>
#include <string>
#include <algorithm>
#include <math.h>
#include <random>
#include <chrono>
#include <functional>   // std::minus
#include <numeric> // inner_product/accumulate
#include <tuple>
#include "linkstream/include/linkStream.hpp"
#include "linkstream/include/community.hpp"
#include "linkstream/include/densityAnalyser.hpp"

#include <string.h>
using namespace gaumont;

static void show_usage(std::string name) {
  std::cerr << "This program compute the scores (start time, duration and node set) of all "
               "groups of links given in parameter."
               "The program create a file res_${g_id} for each group containing the groups caracteristics." << std::endl;
  std::cerr << "Usage: " << name
            << "-i input_file -c comFile"
            << std::endl
            << "Options:\n"
            << "\t-h,--help\t\tShow this help message\n"
            << "\t-i,--input input\t\t file to read 'b e u v'\n"
            << "\t-c comFile "
            << std::endl;
  exit(-1);
}

struct Argument {
  char *inputFile;
  char *comFile;
};

static Argument parse_args(int argc, char **argv) {
  Argument res;
  res.inputFile = NULL;
  res.comFile = NULL;

  if (argc < 2) {
    show_usage(argv[0]);
    exit(-1);
  }

  for (int i = 1; i < argc; ++i) {
    std::string arg = argv[i];
    if ((arg == "-h") || (arg == "--help")) {
      show_usage(argv[0]);
    } else if ((arg == "-i") || (arg == "--input")) {
      if (i + 1 < argc) {
        res.inputFile = argv[++i];
        std::cerr << "InputFile = " << res.inputFile << std::endl;
      } else {
        std::cerr << "--input option requires one argument." << std::endl;
        exit(-1);
      }
    } else if ((arg == "-c")) {
      if (i + 1 < argc) {
        res.comFile = argv[++i];
        std::cerr << "comFile = " << res.comFile << std::endl;
      } else {
        std::cerr << "-c option requires one argument -> comfilePath "
                  << std::endl;
        exit(-1);
      }
    } else {
      show_usage(argv[0]);
    }
  }
  if (res.inputFile == NULL || res.comFile == NULL) {
    show_usage(argv[0]);
    exit(-1);
  }
  return res;
}
template <typename Iter>
std::string join(Iter begin, Iter end, std::string const& separator){
  std::ostringstream result;
  if (begin != end)
    result << *begin++;
  while (begin != end)
    result << separator << *begin++;
  return result.str();
}


void groupEvaluation(const linkStream<Duration> & L, const Group  &g, long double min_duration, long double max_duration, bool is_simple){
   std::string resName = "res_"+ toStr(g.id());
   std::cerr<<"Working on :"<<resName<<std::endl;
   std::ofstream singleEval(resName , std::ios::out | std::ios::trunc);
   if (!singleEval) {
     std::cerr << "Could not create " << resName << "." << std::endl;
     exit(-1);
   }
   singleEval << std::setprecision(16);
   std::set<nodeID> inducedNode;
   for(auto n: g.inducedNode()){
       inducedNode.insert(n->id());
   }

   DensityAnalyser analyser(L, inducedNode, is_simple, false);

   //General stat
   long double time_span = g.lifeTime().end - g.lifeTime().begin;
   long double cur_dens = analyser.between(g.lifeTime().begin, g.lifeTime().end);
   singleEval << "Time: " << g.lifeTime().begin << std::endl;
   singleEval << "Duration: " << time_span << std::endl;
   singleEval << "Nodes: " <<join(inducedNode.begin(),inducedNode.end(),",")<< std::endl;
   singleEval << "Links: " << g.size() << std::endl;
   singleEval << "Cur_dens: " << cur_dens << std::endl;
   // node_score
   long double node_score = analyser.variableNodes(L,g.lifeTime().begin,g.lifeTime().end,is_simple);
   singleEval<<"Node_score: "<<node_score<<std::endl;


   // Duration baseline
    const Profil p1=analyser.variableDuration(min_duration, max_duration, g.lifeTime().begin);
    singleEval<<"Duration_score: "<<p1.percentil_pointwise(cur_dens) <<std::endl;

   // Start baseline
   const Profil p2=analyser.variableStart(L.beginLife() , L.endLife() - time_span,time_span);
   singleEval<<"Start_time_score: "<<p2.percentil_pointwise(cur_dens)<<std::endl;
   singleEval.close();

   // Profil::const_iterator fixTime_max=p1.maxVal(), fixDuration_max=p2.maxVal();
   //Print difference between opt and the group
   // outStream<<time_span<<" "<<g.lifeTime().begin<<" "<<cur_dens<<" "
   //          <<g.lifeTime().begin- (*fixDuration_max).t <<" "
   //          << time_span - (*fixTime_max).t  <<" "
   //          <<cur_dens-(*fixTime_max).val <<" "
   //          <<cur_dens-(*fixDuration_max).val<<std::endl;
}




int main(int argc, char **argv) {
  Argument arg = parse_args(argc, argv);

  gaumont::linkStream<Duration> L;
  L.readFile<BEUV>(arg.inputFile);
  bool is_simple = L.isSimple();

  Cover C(arg.comFile, L);
  long double min_com_dur=std::numeric_limits<long double>::max(),max_com_dur=-1;
  for(auto g: C){
      if(g.size()<10){
          continue;
      }
      timespan span = g.lifeTime();
      long double cur_dur=span.end-span.begin;
      min_com_dur=std::min(cur_dur,min_com_dur);
      max_com_dur=std::max(cur_dur,max_com_dur);
  }
  // long double len = max_com_dur-min_com_dur;
  min_com_dur = 0.8*min_com_dur;
  max_com_dur = 1.2*max_com_dur;

  for(auto g: C){
     if(g.size()<10){
         continue;
     }
     groupEvaluation(L, g, min_com_dur, max_com_dur, is_simple);
  }

  return 0;
}
