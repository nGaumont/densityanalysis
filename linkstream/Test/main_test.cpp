#include "testMaster.hpp"

#include <unistd.h>

using namespace gaumont;
int main() {
  TestMaster test;
  char temp[300];
  getcwd(temp, 300);
  std::cout<<"Testing from "<< std::string(temp)<<std::endl;
  test.all();
  if(test.passed()){
      return 0;
  }else{
      return -1;
  }
}
