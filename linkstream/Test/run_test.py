import PyLinkstream


print("Begin test:")

L = PyLinkstream.Linkstream()
L.readFile(PyLinkstream.SerializationFormat.TUV, "test/test1_TUV.txt")
assert(str([l for l in L.links()])=='[0 1 2 0, 1 2 3 0, 2 2 1 0, 4 2 3 0, 5 3 4 0, 6 2 3 0]')
assert(str(L.link(0))== '0 1 2 0')
assert(str(L.node(1))== '1')
assert(str([n for n in L.nodes()])=='[1, 2, 3, 4]')
assert(str([l for l in L.node(2).links()])=='[0 1 2 0, 1 2 3 0, 2 2 1 0, 4 2 3 0, 6 2 3 0]')
assert(str([l for l in L.activations(L.node(2),L.node(1))]) == '[0 1 2 0, 2 2 1 0]')
assert([e.nodes() for e in L.edges()] == [(1, 2), (2, 3), (3, 4)])
assert(str([l for e in L.edges() for l in e ]) == '[0 1 2 0, 2 2 1 0, 1 2 3 0, 4 2 3 0, 6 2 3 0, 5 3 4 0]')


L = PyLinkstream.Durationstream()
L.readFile(PyLinkstream.SerializationFormat.TUVD, "test/test1.txt")
assert(str([l for l in L.links()])=='[0 1 2 1.5, 1 2 3 2, 2 1 2 1, 4 2 3 1.5, 5 3 4 2, 6 2 3 1]')
assert(str(L.link(0))== '0 1 2 1.5')
assert(str(L.node(1))== '1')
assert(str([n for n in L.nodes()])=='[1, 2, 3, 4]')
assert(str([l for l in L.node(2).links()])=='[0 1 2 1.5, 1 2 3 2, 2 1 2 1, 4 2 3 1.5, 6 2 3 1]')
assert(str([l for l in L.activations(L.node(2),L.node(1))]) == '[0 1 2 1.5, 2 1 2 1]')
assert([e.nodes() for e in L.edges()] == [(1, 2), (2, 3), (3, 4)])
assert(str([l for e in L.edges() for l in e ]) == '[0 1 2 1.5, 2 1 2 1, 1 2 3 2, 4 2 3 1.5, 6 2 3 1, 5 3 4 2]')

C = PyLinkstream.Cover("test/aff_test1.txt",L)
assert(C.checkCoverage(L))
assert(str([g.id() for g in C.groups()])== '[0, 1]')
assert(str([l for l in C.group(1).links()])== '[0 1 2 1.5, 1 2 3 2, 2 1 2 1]')
assert(str([n for n in C.group(1).inducedNodes()]) == '[1, 2, 3]')

D= PyLinkstream.DensityAnalyser(L,True)
assert(D.between(0,7)==1.0/6.0*9.0/7.0)
D.variableStart(1.25, 3.8,2)
D.variableStart(2,5,2)

D2= PyLinkstream.DensityAnalyser(C.group(1), True)
assert(D2.between(0,3)==1.0/3.0*4.5/3.0)

D3 = PyLinkstream.DensityAnalyser(L,[1,2,3,4], False, False)
assert(D.between(0,7)==D3.between(0,7))

P = PyLinkstream.Profil(L, True)
P2 = PyLinkstream.Profil(C.group(1) , True)
P3 = PyLinkstream.Profil(P)

assert(str([b for b in P.blocks()])=="[[0, 1, 1], [1, 0.5, 2], [1.5, 0.5, 1], [2, 1, 2], [3, 1, 0], [4, 1, 1], [5, 0.5, 2], [5.5, 0.5, 1], [6, 1, 2]]")
assert(P.size()==9)
assert(P.AUC(0,7)==9)
assert(P.AUC(0.25,1.25)==1.25)
assert(abs(P.percentil_pointwise(4)-1) < 0.0001)
print(str(P.at(1, True)))
assert(str(P.at(1, True))== "[0, 1, 1]")
assert(str(P.at(1, False))== "[1, 0.5, 2]")
assert(str(P.maxVal())== "[1, 0.5, 2]")
b =P.at(1, True)
print(b.t(),b.d(),b.val())
assert(P==P3)
