#include "densityAnalyser.hpp"

using namespace gaumont;

long double DensityAnalyser::between(timestamp beg, timestamp end)const{
    long double acc = profil.AUC(beg, end);
    return gaumont::density(end-beg, acc, nodes.size());
}



Profil DensityAnalyser::variableStart(timestamp beg_min, timestamp beg_max, long double dur)const{
    assert(beg_max>beg_min);
    // If there is no intersection between [beg_min; beg_max+dur] and profil span then
    //Then there is only an empty profil
    if(beg_min >= profil.endLife() || beg_max+dur <= profil.beginLife()){
        return Profil({{beg_min, Profil::Block(beg_min,beg_max-beg_min,0)},
                       {beg_max, Profil::Block(beg_max,0,0)},
                              });
    }
    unsigned int nodeSize = nodes.size();
    Profil::const_iterator cur_beg=profil.at(beg_min, false),
                           cur_end=profil.at(beg_min+dur, false);
    long double cur_time= beg_min;
    //cur_beg should always contains cur_t
    //cur_end should always contains cur_t+dur;

    long double cum_deg = profil.AUC(cur_time, cur_time+dur);
    Profil p=Profil();

    // if [beg_min, begmin+dur] before the first block (cur_time+dur < (*cur_end).t )
    // or  begmin+dur is after the last possible block ending () AND beg-min is too early.
    if(cur_time+dur < (*cur_end).t  || ((*cur_end).ending() < cur_time+dur && (*cur_beg).t > cur_time )){
        long double cur_dens = density(dur, cum_deg ,nodeSize);
        long double new_curtime;
        if((*cur_end).ending() > cur_time+dur){
            // advance so that cur_end is valid.
            new_curtime = (*cur_end).t-dur;
        }else{
            //cur_end is not a constraint beacause its ending is before the first ending
            // Therefore it should not be considered -> cur_end++ === profil.end().
            new_curtime = (*cur_beg).t;
            ++cur_end;
        }
        p.profil.emplace_hint(p.profil.end(),cur_time, Profil::Block(cur_time, new_curtime - cur_time, cur_dens));
        cur_time = new_curtime;
        // std::cout<<"Décalage "<< p <<std::endl;
        // std::cout<<"D "<< cur_time <<std::endl;
        // std::cout<<"D "<< (*cur_end) <<std::endl;
    }

    if( cur_end!=profil.end() && (*cur_end).ending() < cur_time+dur){
        //cur_end is not a constraint beacause its ending is before the first ending
        // Therefore it should not be considered -> cur_end++ === profil.end().
        ++cur_end;
    }

    // While there is still some time start to explore AND there still some block to process (either beg or end)
    while( cur_time < beg_max  && (cur_end!=profil.end() || cur_beg!=profil.end()) ){
        long double dist_1; // store the time to the next beg which can be
            //(*cur_beg).ending() if cur_time if in cur_beg
            //(*cur_beg).t if cur_time is before in cur_beg
            // Not existent if cur_beg already eached the end.
        if(cur_beg!=profil.end()){
            if((*cur_beg).t <= cur_time){
                dist_1 = std::min((*cur_beg).ending(), beg_max) - cur_time;
            }else{
                dist_1 = (*cur_beg).t - cur_time;
            }
        }else{
            dist_1 = -1;
        }
        long double dist_2= (cur_end!=profil.end() )?  std::min((*cur_end).ending(), beg_max+dur) - (cur_time+dur) : - 1; //time diff whith the next cur_end
        long double dist_min;
        // std::cout<<"B_max"<<beg_max<<std::endl;
        // std::cout<<"C "<<(*cur_beg)<<" "<< cur_time<<"  |  "<<(*cur_end) << " "<< (cur_time+dur)<<std::endl;
        // std::cout<<"C "<<dist_1<<"  |  "<<dist_2<<std::endl;
        if(dist_1>0 ){
            if(dist_2>0){
                // both distance are valid -> advance to the cloest one.
                dist_min = std::min(dist_1, dist_2);
                // std::cout<<"M "<<dist_1 <<" "<<dist_2 <<std::endl;
            }else{
                // only dist1 is valid -> advance to the dist1.
                dist_min = dist_1;
                // std::cout<<"M dist1"<<std::endl;
            }
        }else{
            // only dist2 is valid -> advance to the dist2.
            // dist_2 can't be negative because it would have been manage earlier.
            assert(dist_2>0);
            dist_min = dist_2;
            // std::cout<<"M dist2"<<std::endl;
        }

        // std::cout<<"A "<<cur_time<<" "<< dist_min <<" "<< cum_deg <<std::endl;

        long double cur_dens = density(dur, cum_deg ,nodeSize);
        // std::cout<<"Ajout ["<<cur_time<<","<< dist_min <<","<< cur_dens<<"]" <<std::endl;
        p.profil.emplace_hint(p.profil.end(),cur_time, Profil::Block(cur_time, dist_min, cur_dens));

        if(dist_1>0){
            if((*cur_beg).t <= cur_time){
                // Remove from deg if we actually remove deg from cur_beg
                // This happen if cur_beg is  before cur_time at the beginng.
                cum_deg -= dist_min*(*cur_beg).val;
                if(std::abs(dist_1-dist_min) < RESOLUTION){
                    cur_time = (*cur_beg).ending();
                    ++cur_beg;
                }
            }else{
                if(std::abs(dist_1-dist_min) < RESOLUTION){
                    cur_time = (*cur_beg).t;
                }
            }
        }
        if(dist_2>0){
            cum_deg += dist_min*(*cur_end).val;
        }

        if(dist_2>0 && std::abs(dist_2-dist_min) < RESOLUTION){
            //advance to he next end;
            cur_time = (*cur_end).ending()-dur;
            ++cur_end;
        }
        // std::cout<<"d "<<cum_deg<<std::endl;
        // std::cout<<"A "<<cur_time <<std::endl;
        // std::cout<<std::endl;
    }

    if(cur_time < beg_max){
        // empty block
        // std::cout<<"d "<<cum_deg<<std::endl;
        assert(cum_deg==0);
        p.profil.emplace_hint(p.profil.end(),cur_time, Profil::Block(cur_time, beg_max-cur_time, 0));
        cur_time = beg_max;
    }
    long double cur_dens = density(dur, cum_deg ,nodeSize);
    p.profil.emplace_hint(p.profil.end(),beg_max, Profil::Block(beg_max, 0, cur_dens));
    cur_time = (*cur_end).t-dur;
    return p;
}

Profil DensityAnalyser::variableDuration(long double dur_min, long double dur_max, timestamp beg)const{
    assert(dur_min<dur_max);
    long double end_min = beg+dur_min, end_max=beg+dur_max;
    Profil p=Profil();
    Profil::const_iterator cur=profil.at(beg+dur_min, false),ite= profil.end();
    long double cum_deg = profil.AUC(beg, end_min);
    unsigned int nodeSize = nodes.size();
    long double cur_dur= dur_min;

    //handle if beg is before the first block
    if((*cur).t > end_min){
        // there is no block between end_min and cur.t
        long double span_used= (*cur).t-end_min;
        p.profil.emplace_hint(p.profil.end(),dur_min, Profil::Block(dur_min, span_used, 0));
        cur_dur = (*cur).t-beg;
    }

    //handle when beg is in the middle of the block
    if((*cur).t < end_min){
        long double leng_of_dur = std::min((*cur).ending(),end_max) - end_min;
        long double cur_dens = density(dur_min,cum_deg ,nodeSize);

        p.profil.emplace_hint(p.profil.end(),dur_min, Profil::Block(dur_min, leng_of_dur, cur_dens));
        cum_deg += leng_of_dur * (*cur).val;
        cur_dur += leng_of_dur;
        ++cur;
    }

    while(cur != ite && (*cur).ending()<=end_max){
        long double cur_dens = density(cur_dur,cum_deg ,nodeSize);
        p.profil.emplace_hint(p.profil.end(),cur_dur, Profil::Block(cur_dur, (*cur).d, cur_dens));
        cum_deg += (*cur).d* (*cur).val;
        cur_dur = (*cur).ending()-beg;
        ++cur;
    }
    //Handle the last block
    if(cur != ite && cur_dur+beg <= (*cur).ending() + RESOLUTION && (*cur).t < end_max ){
        long double leng_of_dur = end_max - (*cur).t;
        long double cur_dens = density(cur_dur,cum_deg ,nodeSize);
        p.profil.emplace_hint(p.profil.end(),cur_dur, Profil::Block(cur_dur, leng_of_dur, cur_dens));
        cum_deg += leng_of_dur * (*cur).val;
        cur_dur += leng_of_dur;
    }

    if(cur_dur<dur_max){
        long double cur_dens = density(cur_dur,cum_deg ,nodeSize);
        p.profil.emplace_hint(p.profil.end(),cur_dur, Profil::Block(cur_dur, dur_max-cur_dur, cur_dens));
        cur_dur = dur_max;
    }

    //Compute the density for the last value.
    long double cur_dens = density(dur_max,cum_deg ,nodeSize);
    p.profil.emplace_hint(p.profil.end(),dur_max, Profil::Block(dur_max, 0, cur_dens));
    return p;
}
