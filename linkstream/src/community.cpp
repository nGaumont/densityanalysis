#include "community.hpp"

using namespace gaumont;
bool Group::add(const Link & l){
    bool res = link_set.insert(&l).second;
    if (res){
        updateNodeset = false;
    }
    return res;
}

bool Group::add_hint(linkSet::iterator hint, const Link & l){
    unsigned int old_size = link_set.size();
    link_set.insert(hint, &l);
    if (old_size != link_set.size()){
        updateNodeset = false;
        return true;
    }
    return false;
}

bool Group::remove(const Link & l){
    bool res = static_cast<bool>(link_set.erase(&l));
    if (res){
        updateNodeset = false;
    }
    return res;
}

const std::set<const Node *, nodeComparator > & Group::inducedNode()const{
    if(!updateNodeset){
        node_set.clear();
            for(const Link * l: link_set){
                node_set.insert(&l->left());
                node_set.insert(&l->right());
        }
        updateNodeset = true;
    }
    return node_set;
}

timespan Group::lifeTime()const{
    const Link * first_l = *begin();
    const Link * cur_max = first_l;
    for(const Link * cur_link : *this) {
        if ((cur_link->t + cur_link->duration) > (cur_max->t + cur_max->duration)) {
            cur_max = cur_link;
        }
    }
    return {first_l->t, cur_max->t + cur_max->duration};
}


bool Group::contains(const Link & l)const{
    return link_set.find(&l) != link_set.end();
}

std::ostream &operator<<(std::ostream &out, const Group &g){
    out<<"Group :"<<g.id() <<std::endl;
    for(const Link * l: g){
        out<<*l<<", ";
    }
    return out;
}


Group & Cover::groupContaining(const Link & l){
    auto it = assignation.find(l.id);
    if (it == assignation.end()) {
        std::cerr << "In the cover, there isn't any link with id "<<l.id<<std::endl;
        exit(-1);
    }
    return com_struct[(*it).second];
}



const Group & Cover::group(unsigned int gID)const{
    auto it = com_struct.find(gID);
    if (it == com_struct.end()) {
        std::cerr << "There isn't any group with id "<<gID<<std::endl;
        exit(-1);
    }
    return (*it).second;
}


const Group & Cover::containing(const Link & l)const{
    auto it = assignation.find(l.id);
    if (it == assignation.end()) {
        std::cerr << "In the cover, there isn't any link with id "<<l.id<<std::endl;
        exit(-1);
    }
    return group((*it).second);
}


unsigned int Cover::size()const{
    return com_struct.size();
}


void Cover::moveInto(const Link & l, unsigned int gID){
    Group &g = com_struct[gID];
    Group &g2 = groupContaining(l);
    g2.remove(l);
    if(g2.size()==0){
        com_struct.erase(g2.id());
    }
    g.add(l);
    assignation[l.id] = g.id();
}


void Cover::dumpAffiliation(std::ostream &out)const{
    for(std::map<linkID, unsigned int>::const_iterator it = assignation.begin(); it!= assignation.end();++it){
        out<<(*it).first<<" "<<(*it).second<<std::endl;
    }
}
