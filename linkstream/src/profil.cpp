#include "profil.hpp"
using namespace gaumont;

long double gaumont::density(long double duration, long double sum_deg, unsigned int nodeSize){
    if (duration == 0){
        return 0;
    }
    long double max_link = ((nodeSize-1.0)*nodeSize)/2.0;
    return 1.0/max_link * sum_deg/(duration);
}

std::ostream& gaumont::operator<<(std::ostream & out, const Profil & p){
    out<<"[";
    for(auto it= p.begin();it!= p.end();++it){
        out<<*it<<", ";
    }
    out<<"]";
    return out;
}
void Profil::changementFrom(std::deque<Link *>::const_iterator beg, std::deque<Link *>::const_iterator end, std::map<timestamp, int> & changes)const{
    tmpLink prev_tmp;
    prev_tmp.t=(**beg).t;
    prev_tmp.duration=(**beg).duration;
    ++beg;
    for(;beg!=end;++beg){
        if((**beg).intersection(prev_tmp)>0){
            //Update prev
            prev_tmp.duration = std::max(prev_tmp.t +prev_tmp.duration, (**beg).lifeTime().end)-prev_tmp.t;
        }else{
            increment(changes, prev_tmp.t, 1);
            decrement(changes, prev_tmp.t + prev_tmp.duration, 1);
            prev_tmp.t=(**beg).t;
            prev_tmp.duration=(**beg).duration;
        }
    }
    increment(changes, prev_tmp.t, 1);
    decrement(changes, prev_tmp.t + prev_tmp.duration, 1);
}

void Profil::fillProfil(const std::map<long double, int> & changes, bool clear_profil){
    if (changes.empty()){
        return;
    }
    if(clear_profil){
        profil.clear();
    }
    std::map<long double, int>::const_iterator prev=changes.begin(),cur=prev ;
    ++cur;
    unsigned int cur_deg=0;
    for(;cur!= changes.end();++cur){
        cur_deg += (*prev).second;
        profil.emplace_hint(profil.end(), (*prev).first, Profil::Block((*prev).first, (*cur).first-(*prev).first,cur_deg));
        prev=cur;
    }
    cur_deg += (*prev).second;
    if(cur_deg !=0){
        std::cerr<<"There is a problem with the set of changement provided."<<std::endl;
        exit(-1);
    }
}


std::map<long double, int> Profil::activations(const linkStream<LinkType::Duration> & L, const std::set<nodeID> & nodes)const{
    std::map<long double, int> changes;
    for (auto it = nodes.begin(); it != nodes.end(); ++it) {
        auto it2 = it;
        // I don't consider self loops.
        ++it2;
        for (; it2 != nodes.end(); ++it2) {
            // EventList doit etre appelé avec it < it2
            auto events = L.eventList(*it, *it2); // get all the temporal edges between it and it2
            // first is a bool wether they exist or not.
            // second is an actual links container.
            if (events.first) {
                for (auto it = events.second.begin() ; it !=events.second.end(); ++it) {
                    increment(changes, (**it).t, 1);
                    decrement(changes, (**it).t + (**it).duration, 1);
                }
            }
        }
    }
    return changes;
}

std::map<long double, int> Profil::extended_activations(const linkStream<LinkType::Duration> & L, const std::set<nodeID> & nodes)const{
    std::map<long double, int> changes;
    for (auto it1 = L.beginNodes(); it1 != L.endNodes(); ++it1) {
        auto it2 = nodes.begin();
        for (; it2 != nodes.end(); ++it2) {

            if( (*it2==(*it1).first) || (*it2<=(*it1).first && nodes.find((*it1).first)!= nodes.end())){
                continue;
            }
            nodeID minID= std::min((*it1).first,(*it2)), maxID = std::max(*(it2),(*it1).first);
            // EventList doit etre appelé avec it < it2
            auto events = L.eventList(minID, maxID); // get all the temporal edges between it and it2
            // first is a bool wether they exist or not.
            // second is an actual links container.
            if (events.first) {
                for (auto it = events.second.begin() ; it !=events.second.end(); ++it) {
                    increment(changes, (**it).t, 1);
                    decrement(changes, (**it).t + (**it).duration, 1);
                }
            }
        }
    }
    return changes;
}

std::map<long double, int> Profil::activations_notSimple(const linkStream<LinkType::Duration> & L, const std::set<nodeID> & nodes)const{
    std::map<long double, int> changes;
    for (auto itN = nodes.begin(); itN != nodes.end(); ++itN) {
        auto itN2 = itN;
        // I don't consider self loops.
        ++itN2;
        for (; itN2 != nodes.end(); ++itN2) {
            // EventList doit etre appelé avec it < it2
            auto events = L.eventList(*itN, (*itN2)); // get all the temporal edges between it and it2
            // first is a bool wether they exist or not.
            // second is an actual links container.
            if (events.first) {
                auto itEvent = events.second.begin(), itEventE =events.second.end();
                changementFrom(itEvent, itEventE, changes);
            }
        }
    }
    return changes;
}




std::map<long double, int> Profil::extended_activations_notSimple(const linkStream<LinkType::Duration> & L, const std::set<nodeID> & nodes)const{
    std::map<long double, int> changes;
    for (auto it1 = L.beginNodes(); it1 != L.endNodes(); ++it1) {
        auto it2 = nodes.begin();
        for (; it2 != nodes.end(); ++it2) {

            if( (*it2==(*it1).first) || (*it2<=(*it1).first && nodes.find((*it1).first)!= nodes.end())){
                continue;
            }
            nodeID minID= std::min((*it1).first,(*it2)), maxID = std::max(*(it2),(*it1).first);
            // EventList doit etre appelé avec it < it2
            auto events = L.eventList(minID, maxID); // get all the temporal edges between it and it2
            // first is a bool wether they exist or not.
            // second is an actual links container.
            if (events.first) {
                auto itEvent = events.second.begin(), itEventE =events.second.end();
                changementFrom(itEvent, itEventE, changes);
            }
        }
    }
    return changes;
}

std::map<long double, int> Profil::inter_activations(const linkStream<LinkType::Duration> & L, const Group & g1, const Group &g2, const std::set<nodeID> & union_nodes)const{
    std::map<long double, int> changes;
    timestamp max_g = std::max(g1.lifeTime().end, g2.lifeTime().end);
    timestamp min_g = std::min(g1.lifeTime().begin, g2.lifeTime().begin);
    for (auto it = union_nodes.begin(); it != union_nodes.end(); ++it) {
        auto it2 = it;
        // I don't consider self loops.
        ++it2;
        for (; it2 != union_nodes.end(); ++it2) {
            // EventList doit etre appelé avec it < it2
            auto events = L.eventList(*it, *it2); // get all the temporal edges between it and it2
            // first is a bool wether they exist or not.
            // second is an actual links container.
            if (events.first) {
                for (auto it = events.second.begin() ; it !=events.second.end(); ++it) {
                    if((**it).t> max_g){
                        break;
                    }
                    if((**it).t + (**it).duration< min_g){
                        continue;
                    }
                    if(!g1.contains(**it) &&  !g2.contains(**it)){
                        timestamp cur_min= std::max(min_g,(**it).t );
                        timestamp cur_max= std::min(max_g,(**it).t + (**it).duration);
                        increment(changes, cur_min, 1);
                        decrement(changes, cur_max, 1);
                    }
                }
            }
        }
    }
    return changes;
}

std::map<long double, int> Profil::activations(const Group & g)const{
    Group::const_iterator it= g.begin(), ite = g.end();
    std::map<long double, int> changes;
    for(;it!=ite;++it){
        increment(changes, (**it).t, 1);
        decrement(changes, (**it).t + (**it).duration, 1);
    }
    return changes;
}

std::map<long double, int> Profil::activations_notSimple(const Group & g)const{
    std::map<PairLink, tmpLink, PairLinkComp> events;
    std::map<long double, int> changes;
    Group::const_iterator it= g.begin(), ite = g.end();
    for(;it!=ite;++it){
        nodeID minID=std::min((**it).left().id(),(**it).right().id()), maxID=std::max((**it).left().id(),(**it).right().id());
        tmpLink & last = events[PairLink(minID,maxID)];
        if(last.empty()){
            // First time I see the pair
            last=(*it);
        }else{
            assert((**it).t>= last.t);
            if((**it).t<=last.ending()){
                //Just have to maj the last appearance
                last.duration= std::max((**it).lifeTime().end, last.ending()) - last.t;
            }else{
                increment(changes, last.t, 1);
                decrement(changes, last.ending(), 1);
                last=(*it);
            }
        }
    }

    //Now inside events are last appearance of each (u,v) which have to be stored.
    for(auto pair: events){
        increment(changes, pair.second.t, 1);
        decrement(changes, pair.second.ending(), 1);
    }
    return changes;
}

std::map<long double, int> Profil::inter_activations_notSimple(const linkStream<LinkType::Duration> & L, const Group & g1, const Group &g2, const std::set<nodeID> & union_nodes)const{
    std::map<long double, int> changes;
    std::map<PairLink, tmpLink, PairLinkComp> appear;
    timestamp max_g = std::max(g1.lifeTime().end, g2.lifeTime().end);
    timestamp min_g = std::min(g1.lifeTime().begin, g2.lifeTime().begin);
    for (auto it = union_nodes.begin(); it != union_nodes.end(); ++it) {
        auto it2 = it;
        // I don't consider self loops.
        ++it2;
        for (; it2 != union_nodes.end(); ++it2) {
            // EventList doit etre appelé avec it < it2
            auto events = L.eventList(*it, *it2); // get all the temporal edges between it and it2
            // first is a bool wether they exist or not.
            // second is an actual links container.
            if (events.first) {
                for (auto link_it = events.second.begin() ; link_it !=events.second.end(); ++link_it) {
                    if((**link_it).t> max_g){
                        break;
                    }
                    if((**link_it).t + (**link_it).duration< min_g){
                        continue;
                    }
                    if(!g1.contains(**link_it) &&  !g2.contains(**link_it)){
                        timestamp cur_min= std::max(min_g,(**link_it).t );
                        timestamp cur_max= std::min(max_g,(**link_it).t + (**link_it).duration);
                        tmpLink & last = appear[PairLink(*it, *it2)];
                        if(last.empty()){
                            // First time I see the pair
                            last=(*link_it);
                            last.t=cur_min;
                            last.duration= cur_max - cur_min;
                        }else{
                            assert(cur_min>= last.t);
                            if(cur_min<=last.ending()){
                                //Just have to maj the last appearance
                                last.duration= std::max(cur_max, last.ending()) - last.t;
                            }else{
                                increment(changes, last.t, 1);
                                decrement(changes, last.t + last.duration, 1);
                                last=(*link_it);
                                last.t=cur_min;
                                last.duration= cur_max - cur_min;
                            }
                        }
                    }
                }
            }
        }
    }
    //Now inside events are last appearance of each (u,v) which have to be stored.
    for(auto pair: appear){
        increment(changes, pair.second.t, 1);
        decrement(changes, pair.second.ending(), 1);
    }
    return changes;
}
Profil::Profil(const Group & g, bool assume_simple){
    std::map<long double, int> changes;
    if(assume_simple){
        changes = activations(g);
    }else{
        changes = activations_notSimple( g);
    }

    fillProfil(changes);
}

Profil::Profil(const linkStream<LinkType::Simple> & L, double delta){
    std::map<long double, int> changes;
    typename linkStream<LinkType::Duration>::const_linksIterator it=L.beginLinks(),ite=L.endLinks();
    timestamp min_Life= L.beginLife(),max_Life= L.endLife();
    for(;it!=ite;++it){
        increment(changes, std::min((*it).t-delta, min_Life), 1);
        decrement(changes, std::max((*it).t + delta, max_Life), 1);
    }
    fillProfil(changes);
}
Profil::Profil(const Group & g, double delta){
    std::map<long double, int> changes;
    Group::const_iterator it= g.begin(), ite = g.end();
    timestamp min_Life= g.lifeTime().begin,max_Life= g.lifeTime().end;
    for(;it!=ite;++it){
        increment(changes, std::min((**it).t-delta, min_Life), 1);
        decrement(changes, std::max((**it).t + delta, max_Life), 1);
    }
    fillProfil(changes);
}
Profil::Profil(const linkStream<LinkType::Simple> & L, const std::set<nodeID> & nodes, double delta, bool incluse){
    std::map<long double, int> changes;
    timestamp min_Life= L.beginLife(),max_Life= L.endLife();
    if(incluse){
        for (auto itN = nodes.begin(); itN != nodes.end(); ++itN) {
            auto itN2 = itN;
            // I don't consider self loops.
            ++itN2;
            for (; itN2 != nodes.end(); ++itN2) {
                // EventList doit etre appelé avec it < it2
                auto events = L.eventList(*itN, (*itN2)); // get all the temporal edges between it and it2
                // first is a bool wether they exist or not.
                // second is an actual links container.
                if (events.first) {
                    for (auto it = events.second.begin() ; it !=events.second.end(); ++it) {
                        increment(changes, std::min((**it).t-delta, min_Life), 1);
                        decrement(changes, std::max((**it).t + delta, max_Life), 1);
                    }
                }
            }
        }
    }
    else{
        for (auto it1 = L.beginNodes(); it1 != L.endNodes(); ++it1) {
            auto it2 = nodes.begin();
            for (; it2 != nodes.end(); ++it2) {

                if( (*it2==(*it1).first) || (*it2<=(*it1).first && nodes.find((*it1).first)!= nodes.end())){
                    continue;
                }
                nodeID minID= std::min((*it1).first,(*it2)), maxID = std::max(*(it2),(*it1).first);
                // EventList doit etre appelé avec it < it2
                auto events = L.eventList(minID, maxID); // get all the temporal edges between it and it2
                // first is a bool wether they exist or not.
                // second is an actual links container.
                if (events.first) {
                    for (auto it = events.second.begin() ; it !=events.second.end(); ++it) {
                        increment(changes, std::min((**it).t-delta, min_Life), 1);
                        decrement(changes, std::max((**it).t + delta, max_Life), 1);
                    }
                }
            }
        }
    }

    fillProfil(changes);
}


Profil::Profil(const linkStream<LinkType::Duration> & L, bool assume_simple){
    std::map<long double, int> changes;
    if(assume_simple){
        typename linkStream<LinkType::Duration>::const_linksIterator it=L.beginLinks(),ite=L.endLinks();
        for(;it!=ite;++it){
            increment(changes, (*it).t, 1);
            decrement(changes, (*it).t + (*it).duration, 1);
        }
    }else{
        auto itEvent=L.beginEvent(), iteEvent=L.endEvent();
        for(;itEvent!=iteEvent;++itEvent){
            changementFrom((*itEvent).second.begin(), (*itEvent).second.end(), changes);
        }
    }

    fillProfil(changes);
}


Profil::Profil(const std::deque<Link *> & links):profil(){
    auto it = links.begin(), ite = links.end();
    // timestamp last_begin = 0;
    long double prev_end=0;
    for (; it != ite; ++it) {
        const Link &cur = **it;
        if(cur.t+cur.duration<prev_end){
            std::cerr<<"The linkstream is not simple."<<std::endl;
            exit(-1);
        }
        // std::cout<<std::setprecision(16)<<cur<<std::endl;
        prev_end = cur.t+cur.duration;
        profil.emplace_hint(profil.end(), cur.t, cur);
    }
}


Profil::Profil(timestamp beg, timestamp end, long double val):profil(){
    profil.emplace_hint(profil.end(), beg, Block(beg, end-beg, val));
}
Profil::Profil(const linkStream<LinkType::Duration> & L, const std::set<const Node *, nodeComparator> & nodes, bool incluse, bool assume_simple){
    std::set<nodeID> ids;
    for(auto n : nodes){
        ids.insert(ids.end(), (*n).id());
    }
    std::map<long double, int> changes;
    if(assume_simple){
        if(incluse){
            changes = activations(L, ids);
        }else{
            changes = extended_activations(L,ids);
        }
    }else{
        if(incluse){
            changes = activations_notSimple(L, ids);
        }else{
            changes = extended_activations_notSimple(L,ids);
        }
    }

    fillProfil(changes);
}
Profil::Profil(const linkStream<LinkType::Duration> & L, const std::set<nodeID> & nodes, bool incluse, bool assume_simple){
    std::map<long double, int> changes;
    if(assume_simple){
        if(incluse){
            changes = activations(L, nodes);
        }else{
            changes = extended_activations(L,nodes);
        }
    }else{
        if(incluse){
            changes = activations_notSimple(L, nodes);
        }else{
            changes = extended_activations_notSimple(L,nodes);
        }
    }
    fillProfil(changes);
}

bool Profil::operator==(const Profil & p) const{
    if(profil.size() != p.profil.size()){
        return false;
    }
    Profil::const_iterator cur1=begin(), cur2=p.begin(),
                     end1=end(),end2=p.end();
    while(cur1!=end1 && cur2 !=end2){
        if((*cur1)!=(*cur2)){
            // std::cout<<"Dif:"<<(*cur1)<<" "<<(*cur2)<<std::endl;
            // std::cout<<std::setprecision(48);
            // std::cout<<RESOLUTION<<std::endl;
            // std::cout<<(*cur1).t<<" "<<(*cur2).t<<" "<<(*cur1).t - (*cur2).t<<std::endl;
            // std::cout<<(*cur1).d<<" "<<(*cur2).d<<" "<<(*cur1).d-(*cur2).d<<std::endl;
            // std::cout<<(*cur1).val<<" "<<(*cur2).val<<" "<<(*cur1).val-(*cur2).val<<std::endl;
            return false;
        }
        ++cur1;
        ++cur2;
    }
    return true;
}

void Profil::add(const Profil & p){
    this->modifier(p,[](const Block & x, const Block & y){return x.val+y.val;});
}

Profil::iterator Profil::modifier_hint(Profil::iterator hint, Block b, std::function<long double (const Block &, const Block &)> func){
    // assert(endLife() >= b.ending());
    if(!(*hint).intersect(b)){
        std::cerr<< std::setprecision(48);
        std::cerr<<"Wrong hint "<<*hint<<" "<<b<<std::endl;
        std::cerr<<"Wrong hint "<<b<<std::endl;
        std::cerr<<" "<<(*hint).isconsistent()<<std::endl;
        std::cerr<<" "<<(*this).isconsistent()<<std::endl;
        std::cerr<<" "<<(*this)<<std::endl;
        std::cerr<<"Wrong hint "<<(*hint).t-b.t<<" "<<(*hint).ending()-b.t <<std::endl;
        exit(-1);
    }
    long double time_dif = b.t-(*hint).t;
    // b and hint don't begin dont begin at the same time.
    if(time_dif > RESOLUTION){
        //split hint
        long double last_val = (*hint).val;
        long double last_end = (*hint).ending();
        assert(last_end-b.t > 0);
        (*hint).d=time_dif;
        ++hint;
        hint = profil.emplace_hint(hint.raw(), b.t, Block(b.t, last_end-b.t, last_val));

    }
    // now hint and b have the same start time.
    // Only have to update val for block which are inside the time interval from b.
    while( hint!=end()&& (*hint).ending() <= b.ending()){
        (*hint).val = func(*hint, b);
        ++hint;
    }

    //Now hint migh outlive and has been mistakenly updated
    // first check that b is not already finsished then
    if( hint!=end() && b.ending()-(*hint).t> RESOLUTION  ){
        long double last_val = (*hint).val;

        assert(b.ending()-(*hint).t > 0);
        (*hint).val = func(*hint, b);
        long double last_dur = (*hint).ending()-b.ending();
        assert(last_dur > 0);
        if (last_dur>RESOLUTION){
            (*hint).d = b.ending()-(*hint).t;
            ++hint;
            hint = profil.emplace_hint(hint.raw(), b.ending(), Block(b.ending(), last_dur, last_val));
        }else{
            ++hint;
        }
    }

    return hint;
}
void Profil::modifier(const Profil & p, std::function<long double (const Block &, const Block &)> func){
    // assert(this->isconsistent());
    // assert(p.isconsistent());
    Profil::const_iterator pit =p.begin(), pite=p.end();
    Profil::iterator cur=at((*pit).t); // Return closest block to t.
    pit = p.at((*cur).t,false);
    while(pit != pite && cur != end()){
        cur = modifier_hint(cur,*pit, func);
        ++pit;
        // advance cur to the next intersction
        while((*cur).ending()<= (*pit).t && cur != end()){
            ++cur;
        }
    }
    // std::cout<<"Result profil:"<<std::endl;
    // std::cout<<*this<<std::endl;
}

void Profil::modifier(std::function<long double (const Block &)> func){
    assert(this->isconsistent());
    Profil::iterator it =begin(), ite=end();
    while(it != ite){
        (*it).val = func(*it);
        ++it;
    }
}

long double Profil::AUC(timestamp beg, timestamp end)const{
    if(beg> end){
        return 0.0;
    }
    assert(end>beg);
    if((*this).size() == 0 || end <= beginLife() || beg >= endLife() ){
        return 0;
    }
    Profil::const_iterator cur = at(beg,false);
    long double acc = 0.0;
    if(end <= (*cur).ending()){
        return (*cur).val *(std::min(end,(*cur).ending())-std::max(beg,(*cur).t));
    }

    // The first block is used only in the interval [max(beg,cur.t), cur.ending]
    acc += (*cur).val * ((*cur).ending() - std::max(beg,(*cur).t));
    ++cur;
    while(cur != profil.end() && (*cur).ending() < end - RESOLUTION){
        acc += (*cur).val* (*cur).d ;
        ++cur;
    }
    if(cur != profil.end()){
        // the last block is also only used in the interval [cur.t; end]
        acc += (*cur).val * (std::min(end,(*cur).ending())  - (*cur).t);
    }
    return acc;
}


void Profil::dump()const{
    Profil::const_iterator it = begin(), ite = end();
    for(;it!=ite;++it){
        std::cout<<(*it).t<<" "<<(*it).val<<std::endl;
        std::cout<<(*it).ending()<<" "<<(*it).val<<std::endl;
    }
}

long double Profil::percentil_pointwise(long double threshold)const{
    assert(size()>=2);
    long double res =0;
    Profil::const_iterator it = begin(), next=it, ite=end();
    ++next;
    for(;next!=ite;++next){
        // The block represent has a value from b.val to next.val in the interval of b.
        const Block & b=*it;
        const Block & be=(*next);

        if(b.val < threshold){
            if(be.val >= threshold){
                // b is under and next is above
                long double intersect_x = (be.t-b.t)*(threshold-b.val)/(be.val-b.val) + b.t;
                res+= be.t-intersect_x;
            }
            // If both are under -> nothing to do
        }else{
            if(be.val < threshold){
                // b is above and next under
                long double intersect_x = (be.t-b.t)*(threshold-b.val)/(be.val-b.val) + b.t;
                res+= intersect_x-b.t;
            }else{
                // Both  b and next are above
                res+= b.d;
            }
        }
        it=next;
    }
    res = 1-res/(span());
    return res;
}

long double Profil::percentil_stepFunction(long double threshold)const{
    assert(size()>=2);
    long double res =0;
    Profil::const_iterator it = begin(), ite=end();
    for(;it!=ite;++it){
        // The block represent has a value from b.val to next.val in the interval of b.
        const Block & b=*it;
        if(b.val >= threshold){
                // Both  b and next are above
                res+= b.d;
        }
    }
    res = 1-res/(span());
    return res;
}

bool Profil::isconsistent()const{
    if(size()==0){
        return true;
    }
    if(size()==1){
        return (*begin()).isconsistent();
    }
    Profil::const_iterator it = begin(), next=it, ite=end();
    ++next;
    for(;next!=ite;++next){
        if( ! (*it).isconsistent() || std::abs((*it).ending()-(*next).t)>RESOLUTION ){
            std::cout<<(*it)<<" and "<<*next<<" are not consistent."<<std::endl;
            return false;
        }
        it=next;
    }
    if(!(*it).isconsistent()){
        std::cout<<(*it)<<" is not consistent. "<<std::isfinite((*it).val)<<std::endl;
    }

    return (*it).isconsistent();
}



double Profil::value_at(double t)const{
    auto it = at(t);
    auto next = it;
    ++next;
    if (next == cend()){
        std::cerr << "Trop loin pour le sampling"<< std::endl;
        exit(-1);
    }
    return ((*next).val-(*it).val) * (t - (*it).t)/((*next).t - (*it).t) + (*it).val;
}
