#include "testMaster.hpp"

using namespace gaumont;


bool TestMaster::testLinkStream(){
    std::cout<<std::setprecision(16);
    std::cout<<"---- LinkStream -----"<<std::endl;
    linkStream<Duration> L1;
    L1.readFile<TUVD>("test/test1.txt");
    std::cout<<"Test constructor EventFormat::TUVD():"<<std::endl;
    std::cout<<"test1.txt TUVD(): " << single(L1.compLinkList({tmpLink(0,0,nodeID(1),nodeID(2),1.5),
                                                               tmpLink(1,1,nodeID(2),nodeID(3),2),
                                                               tmpLink(2,2,nodeID(1),nodeID(2),1),
                                                               tmpLink(3,4,nodeID(2),nodeID(3),1.5),
                                                               tmpLink(4,5,nodeID(3),nodeID(4),2),
                                                               tmpLink(5,6,nodeID(2),nodeID(3),1),})==true)<<std::endl;
   linkStream<Duration> L1_BEUV;
   L1_BEUV.readFile<BEUV>("test/test1_beuv.txt");
   std::cout<<"Test constructor EventFormat::BEUV():"<<std::endl;
   std::cout<<"test1.txt BEUV(): " << single(L1_BEUV.compLinkList({tmpLink(0,0,nodeID(1),nodeID(2),1.5),
                                                              tmpLink(1,1,nodeID(2),nodeID(3),2),
                                                              tmpLink(2,2,nodeID(1),nodeID(2),1),
                                                              tmpLink(3,4,nodeID(2),nodeID(3),1.5),
                                                              tmpLink(4,5,nodeID(3),nodeID(4),2),
                                                              tmpLink(5,6,nodeID(2),nodeID(3),1),})==true)<<std::endl;

      linkStream<Simple> L3_TUV;
      L3_TUV.readFile<TUV>("test/test1_TUV.txt");
      std::cout<<"Test constructor EventFormat::TUV():"<<std::endl;
      std::cout<<"test1.txt TUV(): " << single(L3_TUV.compLinkList({tmpLink(0,0,nodeID(1),nodeID(2),0),
                                                                tmpLink(1,1,nodeID(2),nodeID(3),0),
                                                                tmpLink(2,2,nodeID(2),nodeID(1),0),
                                                                tmpLink(3,4,nodeID(2),nodeID(3),0),
                                                                tmpLink(4,5,nodeID(3),nodeID(4),0),
                                                                tmpLink(5,6,nodeID(2),nodeID(3),0),}))<<std::endl;
    linkStream<Simple> L4_TUV;
    L4_TUV.readFile<TUV>("test/test1_TUV.txt");
    std::cout<<"test1.txt TUV(): " << single(L4_TUV.compLinkList(L3_TUV))<<std::endl;

    linkStream<Duration> L1_TUV;
    L1_TUV.readFile<TUV>("test/test1_TUV.txt",2);
    std::cout<<"test1.txt TUV(): " << single(L1_TUV.compLinkList({tmpLink(0,-1,nodeID(1),nodeID(2),2),
                                                              tmpLink(1,0,nodeID(2),nodeID(3),2),
                                                              tmpLink(2,1,nodeID(2),nodeID(1),2),
                                                              tmpLink(3,3,nodeID(2),nodeID(3),2),
                                                              tmpLink(4,4,nodeID(3),nodeID(4),2),
                                                              tmpLink(5,5,nodeID(2),nodeID(3),2),}))<<std::endl;
    linkStream<Duration> L2_TUV;
    L2_TUV.readFile<TUV>("test/test1_TUV.txt",4);
    std::cout<<"test1.txt TUV(): " << single(L2_TUV.compLinkList({tmpLink(0,-2,nodeID(1),nodeID(2),4),
                                                            tmpLink(1,-1,nodeID(2),nodeID(3),4),
                                                            tmpLink(2,0,nodeID(2),nodeID(1),4),
                                                            tmpLink(3,2,nodeID(2),nodeID(3),4),
                                                            tmpLink(4,3,nodeID(3),nodeID(4),4),
                                                            tmpLink(5,4,nodeID(2),nodeID(3),4)}))<<std::endl;
    std::cout<<"Test isSimple():"<<std::endl;
    std::cout<<"test1.txt isSimple(): " << single(L1.isSimple()==true)<<std::endl;

    linkStream<Duration> L2;
    L2.readFile<TUVD>("test/test2.txt");
    std::cout<<"test2.txt isSimple(): " << single(L2.isSimple()==false)<<std::endl;
    {
        linkStream<Duration> L6;
        L6.readFile<TUV>("test/test6_TUV.txt",0.0001);
        std::cout<<"test6_TUV isSimple(): " << single(L6.isSimple()==false)<<std::endl;
        linkStream<Duration> L6_simple= L6.makeSimple();
        linkStream<Duration> L7;
        L7.readFile<TUVD>("test/test6_TUVSimple.txt");
        std::cout<<"test6_TUV makeSimple(): " << single(L6_simple.compLinkList(L7)==true)<<std::endl;
    }

    std::cout<<"Test makeSimple():"<<std::endl;
    linkStream<Duration> L3;
    L3.readFile<TUVD>("test/test3.txt");
    std::cout<<"test2.txt: " << single(L2.makeSimple().compLinkList(L3)==true)<<std::endl;

    std::cout<<"test1.txt makeSimple(): " << single(L1.makeSimple().compLinkList(L1)==true)<<std::endl;

    {
      std::ifstream input("test/test4.txt");
      if (!input) {
        std::cerr << "The file " << "test/test4.txt" << " does not exist" << std::endl;
        exit(-1);
      }
      linkStream<Simple> L4(10);
      while(L4.readBuffer<TUV>(input)){
      }
      input.close();
      std::cout<<"readBuffer<EventFormat::TUV>(): " << single(L4.compLinkList({tmpLink(383,575922114,nodeID(41642),nodeID(50451),0),
                                                               tmpLink(384,577393313,nodeID(47989),nodeID(32869),0),
                                                               tmpLink(385,580075661,nodeID(50067),nodeID(49645),0),
                                                               tmpLink(386,581798004,nodeID(46360),nodeID(51077),0),
                                                               tmpLink(387,582497522,nodeID(50871),nodeID(51070),0),
                                                               tmpLink(388,589755065,nodeID(45256),nodeID(51052),0),
                                                               tmpLink(389,595729843,nodeID(26158),nodeID(51358),0),
                                                               tmpLink(390,596307939,nodeID(51408),nodeID(50636),0),
                                                               tmpLink(391,598393853,nodeID(49206),nodeID(51243),0),
                                                               tmpLink(392,600020087,nodeID(43849),nodeID(47176),0)}))<<std::endl;
    }
    std::cout<<"test1.txt: " << single(L1.makeSimple().compLinkList(L1)==true)<<std::endl;
    std::cout<<"test1_TUV.txt: " << single(L2_TUV.makeSimple().compLinkList({tmpLink(0,-2,nodeID(1),nodeID(2),6),
                                                            tmpLink(1,-1,nodeID(2),nodeID(3),9),
                                                            tmpLink(2,3,nodeID(3),nodeID(4),4)}))<<std::endl;

    {
        std::cout<<"link():"<< single(L1.link(0).weakEqual(tmpLink(0,0,nodeID(1),nodeID(2),1.5)))<<std::endl;

        std::cout<<"node():"<< single(L1.node(1).id()==1)<<std::endl;
        std::deque<tmpLink> good({tmpLink(0,0,nodeID(1),nodeID(2),1.5),
                                                     tmpLink(1,1,nodeID(2),nodeID(3),2),
                                                     tmpLink(2,2,nodeID(1),nodeID(2),1),
                                                     tmpLink(3,4,nodeID(2),nodeID(3),1.5),
                                                     tmpLink(5,6,nodeID(2),nodeID(3),1)});
        bool res =true;
        auto it2 = good.begin();

        for(const Link * l :L1.node(2).linksNeighbor()){
            if(!(*l).weakEqual(*it2)){
                std::cout<< *l<<" != "<< *it2<<std::endl;
                res = false;
            }
            ++it2;
        }
        assert(L1.node(2).linksNeighbor().size()== good.size());
        std::cout<<"node.linksNeighbor():"<< single(res)<<std::endl;
    }

    {
      std::deque<tmpLink> good({tmpLink(0,0,nodeID(1),nodeID(2),1.5),
                                tmpLink(1,1,nodeID(2),nodeID(3),2),
                                tmpLink(2,2,nodeID(1),nodeID(2),1),
                                tmpLink(3,4,nodeID(2),nodeID(3),1.5),
                                tmpLink(5,6,nodeID(2),nodeID(3),1)});
      auto it2 = good.begin();
      std::deque<Link *> neighbor;
      L1.link(0).linkNeighbor(neighbor);
      bool res =true;
      for(const Link * l : neighbor){
          if(!(*l).weakEqual(*it2)){
             std::cout<<"Diff "<< *l<<" "<< *it2<<std::endl;
              res = false;
          }
          ++it2;
      }
      assert(neighbor.size()== good.size());
      std::cout<<"link.linksNeighbor():"<< single(res)<<std::endl;
   }
   {
     std::deque<tmpLink> good({tmpLink(1,1,nodeID(2),nodeID(3),2),
                              tmpLink(3,4,nodeID(2),nodeID(3),1.5),
                              tmpLink(4,5,nodeID(3),nodeID(4),2),
                              tmpLink(5,6,nodeID(2),nodeID(3),1)});
     auto it2 = good.begin();
     std::deque<Link *> neighbor;
     L1.link(4).linkNeighbor(neighbor);
     bool res =true;
     for(const Link * l : neighbor){
        if(!(*l).weakEqual(*it2)){
             res = false;
             std::cout<<"Diff "<< *l<<" "<< *it2<<std::endl;
        }
        ++it2;
     }
     assert(neighbor.size()== good.size());
     std::cout<<"link.linksNeighbor():"<< single(res)<<std::endl;
 }
    return true;
}
bool TestMaster::testCommunity(){
    {
        std::cout<<"---- Community -----"<<std::endl;
        linkStream<Duration> L;
        L.readFile<TUVD>("test/test1.txt");
        Cover C("test/aff_test1.txt", L);
        std::set<nodeID> tmp;
        for( const Node * n : C.group(0).inducedNode()){
            tmp.insert((*n).id());
        }
        std::cout<<"inducedNode():"<< single(tmp==std::set<nodeID>({nodeID(2),nodeID(3),nodeID(4)}))<<std::endl;
    }
    return true;
}

bool TestMaster::testProfil(){
    linkStream<Duration> L;
    L.readFile<TUVD>("test/test1.txt");
    Cover C("test/aff_test1.txt", L);
    std::cout<<std::setprecision(16);
    std::cout<<"Test activation:"<<std::endl;
    {
        Profil p= Profil();
        std::map<long double, int> tmp = {{0,1}, {1,1}, {1.5,-1}, {2,1}, {3,-2}, {4,1}, {5,1}, {5.5,-1}, {6,1}, {7,-2}};
        std::cout<<"activation(L, nodes):" << single(p.activations(L, {nodeID(1),nodeID(2),nodeID(3),nodeID(4)})==tmp)<<std::endl;
        p.fillProfil(tmp);
        Profil tmp2 =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.5,2)},
                              {1.5, Profil::Block(1.5,0.5,1.0)},
                              {2, Profil::Block(2,1,2)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1,1)},
                              {5, Profil::Block(5,0.5,2)},
                              {5.5, Profil::Block(5.5,0.5,1)},
                              {6, Profil::Block(6,1,2)},
                          });
        std::cout<<"fillProfil():" << single(p==tmp2)<<std::endl;
        Profil p2(L, {nodeID(1),nodeID(2),nodeID(3),nodeID(4)}, true, true);
        Profil p3(L,true);
        Profil p4(L, false);
    }
    {
        Profil p= Profil();
        std::map<long double, int> tmp = {{1,1}, {3,-1}, {4,1}, {5,1}, {5.5,-1}, {6,1}, {7,-2}};
        std::cout<<"activation(L, nodes-1)_bis:" << single(p.activations(L, {nodeID(2),nodeID(3),nodeID(4)})==tmp)<<std::endl;
        p.fillProfil(tmp);
        Profil tmp2 =  Profil({{1, Profil::Block(1,2,1)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1,1)},
                              {5, Profil::Block(5,0.5,2)},
                              {5.5, Profil::Block(5.5,0.5,1)},
                              {6, Profil::Block(6,1,2)},
                          });
        std::cout<<"fillProfil()_bis:" << single(p==tmp2)<<std::endl;
        Profil p2(L, {nodeID(2),nodeID(3),nodeID(4)}, true, true);
        std::cout<<"Profil(L,nodes)_bis:" << single(p==p2)<<std::endl;
    }
    {
        Profil p= Profil();
        std::map<long double, int> tmp = {{0,1}, {1,1}, {1.5,-1}, {2,1}, {3,-2}, {4,1}, {5.5,-1}, {6,1}, {7,-1}};
        std::cout<<"activation(L, nodes-1):" << single(p.activations(L, {nodeID(1),nodeID(2),nodeID(3)})==tmp)<<std::endl;
        p.fillProfil(tmp);
        Profil tmp2 =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.5,2)},
                              {1.5, Profil::Block(1.5,0.5,1.0)},
                              {2, Profil::Block(2,1,2)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1.5,1)},
                              {5.5, Profil::Block(5.5,0.5,0)},
                              {6, Profil::Block(6,1,1)},
                          });
        std::cout<<"fillProfil():" << single(p==tmp2)<<std::endl;
        Profil p2(L, {nodeID(1),nodeID(2),nodeID(3)}, true, true);
        std::cout<<"Profil(L,nodeID):" << single(p==p2)<<std::endl;
        Profil p3(L, {&L.node(1),&L.node(2), &L.node(3)}, true, true);
        std::cout<<"Profil(L,nodes):" << single(p3==p2)<<std::endl;
    }
    {
        Profil p= Profil();
        std::map<long double, int> tmp = {{0,1}, {1,1}, {1.5,-1}, {2,1}, {3,-2}, {4,1}, {5.5,-1}, {6,1}, {7,-1}};
        std::cout<<"extended_activation(L, nodes-2):" << single(p.extended_activations(L, {nodeID(1),nodeID(2)})==tmp)<<std::endl;
        p.fillProfil(tmp);
        Profil tmp2 =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.5,2)},
                              {1.5, Profil::Block(1.5,0.5,1.0)},
                              {2, Profil::Block(2,1,2)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1.5,1)},
                              {5.5, Profil::Block(5.5,0.5,0)},
                              {6, Profil::Block(6,1,1)},
                          });
        std::cout<<"fillProfil():" << single(p==tmp2)<<std::endl;
        Profil p2(L, std::set<nodeID>({nodeID(1),nodeID(2)}), false, true);
        std::cout<<"Profil(L,nodes,false, true):" << single(p==p2)<<std::endl;
    }

    {
        linkStream<Duration> L;
        L.readFile<TUVD>("test/test2.txt");
        std::cout<<std::setprecision(16);
        std::set<nodeID> nodes= {nodeID(2), nodeID(3)};
        Profil p1(L, nodes, false, false);
        std::cout<<"Profil(L,nodes,false, false):" << single(p1==Profil(L,false))<<std::endl;
    }
    {
        linkStream<Duration> L;
        L.readFile<TUVD>("test/test2.txt");
        std::cout<<std::setprecision(16);
        std::set<nodeID> nodes= {nodeID(1), nodeID(2)};
        Profil p1(L, nodes, true, false);
        Profil tmp1 =  Profil({{0, Profil::Block(0,3,1.0)},
                          });
        std::cout<<"Profil(L,nodes,true, false):" << single(p1==tmp1)<<std::endl;
    }

    {
        Profil p(C.group(0));
        Profil tmp2 =  Profil({{4, Profil::Block(4,1,1)},
                              {5, Profil::Block(5,0.5,2)},
                              {5.5, Profil::Block(5.5,0.5,1)},
                              {6, Profil::Block(6,1,2)},
                          });
        std::cout<<"Profil(L, group):" << single(p==tmp2)<<std::endl;
    }
    {
        Cover C2("test/aff_test2.txt", L);
        Profil p;
        std::map<long double, int> tmp = {{2,1}, {3,-1}, {4,1}, {5.5,-1}},
                                   res = p.inter_activations(L, C2.group(0), C2.group(2), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)});
        std::cout<<"activation(L,inter):" << single(res==tmp)<<std::endl;
        p.fillProfil(tmp);
        Profil tmp2 =  Profil({{2, Profil::Block(2,1,1)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1.5,1)},
                          });
        std::cout<<"fillProfil():" << single(p==tmp2)<<std::endl;
        Profil p2 = Profil::inter(L, C2.group(0), C2.group(2), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)});
        std::cout<<"Profil::inter:" << single(p==p2)<<std::endl;
    }
    {
        Cover C2("test/aff_test2.txt", L);
        Profil p = Profil::inter(L, C2.group(1), C2.group(2), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)});
        Profil tmp2 =  Profil({{2, Profil::Block(2,1,1)}
                          });
        std::cout<<"Profil::inter:" << single(p==tmp2)<<std::endl;
    }
    {
        Cover C2("test/aff_test2.txt", L);
        Profil p(C2.group(1),false);
        Profil p2(C2.group(1),true);
        std::cout<<"Profil::Profil(L,group,false):" << single(p==p2)<<std::endl;
    }
    {
        linkStream<Duration> L1_TUV;
        L1_TUV.readFile<TUV>("test/test1_TUV.txt",2);
        Cover C1_TUV("test/aff_test1.txt", L1_TUV);
        Profil p(C1_TUV.group(1),false);
        Profil tmp2 =  Profil({{-1, Profil::Block(-1,1,1)},
                              {0, Profil::Block(0,2,2)},
                              {2, Profil::Block(2,1,1)},
                          });
        std::cout<<"Profil::notsimple activation_1:" << single(p==tmp2)<<std::endl;
    }
    {
        linkStream<Duration> L2_TUV;
        L2_TUV.readFile<TUV>("test/test1_TUV.txt",4);
        Cover C2_TUV("test/aff_test2.txt", L2_TUV);
        Profil p(C2_TUV.group(1),false);
        Profil tmp2 =  Profil({{0, Profil::Block(0,2,1)},
                              {2, Profil::Block(2,2,2)},
                              {4, Profil::Block(4,2,1)}
                          });
        std::cout<<"Profil::notsimple activation_2:" << single(p==tmp2)<<std::endl;
    }
    {
        linkStream<Duration> L2_TUV;
        L2_TUV.readFile<TUV>("test/test1_TUV.txt",4);
        Profil p(L2_TUV,false);
        Profil p2(L2_TUV.makeSimple(),true);
        std::cout<<"Profil::Profil(L, false):" << single(p==p2)<<std::endl;
    }
    {
        linkStream<Duration> L2_TUV;
        L2_TUV.readFile<TUV>("test/test1_TUV.txt",1);
        Cover C2("test/aff_test2.txt", L2_TUV);
        Profil p = Profil::inter(L2_TUV, C2.group(1), C2.group(2), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)},true);
        Profil p2 = Profil::inter(L2_TUV, C2.group(1), C2.group(2), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)},false);
        std::cout<<"Profil::inter not simple_1:" << single(p==p2)<<std::endl;
        // std::cout<<"Profil::inter_activations_notSimple:" << single(res==tmp)<<std::endl;
    }
    {
        linkStream<Duration> L2_TUV;
        L2_TUV.readFile<TUVD>("test/test1.txt");
        Cover C3("test/aff_test3.txt", L2_TUV);
        Profil p(C3.group(2),true);
        Profil tmp2 =  Profil({{2, Profil::Block(2,1,1)}
                          });
        std::cout<<"Profil(group):" << single(p==tmp2)<<std::endl;
    }
    {
        linkStream<Duration> L2_TUV;
        L2_TUV.readFile<TUV>("test/test1_TUV.txt",4);
        Cover C3("test/aff_test3.txt", L2_TUV);
        Profil p = Profil::inter(L2_TUV, C3.group(2), C3.group(4), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)},false);
        Profil tmp2 =  Profil({{0, Profil::Block(0,2,2)},
                              {2, Profil::Block(2,5,1)}
                          });
        std::cout<<"Profil::inter not simple_2:" << single(p==tmp2)<<std::endl;
    }
    {
        linkStream<Duration> L2_TUV;
        L2_TUV.readFile<TUV>("test/test1_TUV.txt",4);
        Cover C3("test/aff_test3.txt", L2_TUV);
        Profil p = Profil::inter(L2_TUV, C3.group(2), C3.group(5), {nodeID(1),nodeID(2),nodeID(3)},false);
        Profil tmp2 =  Profil({{0, Profil::Block(0,2,2)},
                              {2, Profil::Block(2,4,1)}
                          });
        std::cout<<"Profil::inter not simple_3:" << single(p==tmp2)<<std::endl;
    }
    {
        linkStream<Duration> L2_TUV;
        L2_TUV.readFile<TUV>("test/test2_TUV.txt",4);
        Cover C3("test/aff_test3.txt", L2_TUV);
        Profil p = Profil::inter(L2_TUV, C3.group(2), C3.group(5), {nodeID(1),nodeID(2),nodeID(3)},false);
        Profil tmp2 =  Profil({{0, Profil::Block(0,2,2)},
                              {2, Profil::Block(2,4,1)},
                          });
        std::cout<<"Profil::inter not simple_4:" << single(p==tmp2)<<std::endl;
    }

   return true;
}
bool TestMaster::testDensityAnalyser(){
    linkStream<Duration> L;
    L.readFile<TUVD>("test/test1.txt");
    std::cout<<std::setprecision(16);
    std::set<nodeID> nodes;

    for(auto it = L.beginNodes(); it !=L.endNodes();++it){
        nodes.insert(it->first);
    }
    DensityAnalyser d(L,nodes, true, true);

    std::cout<<"---- Profil -----"<<std::endl;
    {
        Profil tmp =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.5,2)},
                              {1.5, Profil::Block(1.5,0.5,1.0)},
                              {2, Profil::Block(2,1,2)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1,1)},
                              {5, Profil::Block(5,0.5,2)},
                              {5.5, Profil::Block(5.5,0.5,1)},
                              {6, Profil::Block(6,1,2)},
                          });
        std::cout<<"Construction:"<<single(d.profil == tmp )<<std::endl;
        std::cout<<"isconsistent:"<<single(d.profil.isconsistent())<<std::endl;
        DensityAnalyser d2(L,true);
        std::cout<<"Construction2:"<<single(d.profil == d2.profil )<<std::endl;
        // std::cout<<d.profil<<std::endl<<tmp<<std::endl;
    }
    std::cout<<std::endl;

    std::cout<<"Test at:"<<std::endl;
    std::cout<<"at(0):" << single(*d.profil.at(0)==Profil::Block(0,1.0,1.0))<<std::endl;
    std::cout<<"at(-3):" << single(*d.profil.at(-3)==Profil::Block(0,1.0,1.0))<<std::endl;
    std::cout<<"at(0.5):" << single(*d.profil.at(0.5)==Profil::Block(0,1.0,1.0))<<std::endl;
    std::cout<<"at(0.5,false):"<< single(*d.profil.at(0.5,false)==Profil::Block(0,1.0,1.0))<<std::endl;
    std::cout<<"at(1):"<< single(*d.profil.at(1)==Profil::Block(0,1.0,1.0))<<std::endl;
    std::cout<<"at(1,false):"<< single(*d.profil.at(1,false)==Profil::Block(1,0.5,2.0))<<std::endl;
    std::cout<<"at(7):"<< single(*d.profil.at(7)==Profil::Block(6,1,2.0))<<std::endl;
    std::cout<<"at(7,false):"<< single(*d.profil.at(7,false)==Profil::Block(6,1,2.0))<<std::endl;
    std::cout<<"at(8):"<< single(*d.profil.at(8)==Profil::Block(6,1,2.0))<<std::endl;
    std::cout<<"at(8,false):"<< single(*d.profil.at(8,false)==Profil::Block(6,1,2.0))<<std::endl;
    std::cout<<std::endl;

    {
        Profil tmp2 =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.5,4)},
                              {1.5, Profil::Block(1.5,0.5,3)},
                              {2, Profil::Block(2,1,4)},
                              {3, Profil::Block(3,1,2)},
                              {4, Profil::Block(4,0.5,1)},
                              {4.5, Profil::Block(4.5,0.2,2)},
                              {4.7, Profil::Block(4.7,0.3,1)},
                              {5, Profil::Block(5,0.25,2)},
                              {5.25, Profil::Block(5.25,0.25,3)},
                              {5.5, Profil::Block(5.5,0.5,2)},
                              {6, Profil::Block(6,0.25,3)},
                              {6.25, Profil::Block(6.25,0.75,2)},
                              });
        Profil p_(d.profil);
        Profil::iterator it = p_.at(1.2, false);
        p_.modifier_hint(it, Profil::Block(1,3,2.0),[](const Profil::Block & x, const Profil::Block & y) {return x.val+y.val;});
        p_.modifier_hint(p_.at(4.5, false), Profil::Block(4.5,0.2,1),[](const Profil::Block & x, const Profil::Block & y) {return x.val+y.val;});
        p_.modifier_hint(p_.at(5.25, false), Profil::Block(5.25,1,1.0),[](const Profil::Block & x, const Profil::Block & y) {return x.val+y.val;});
        std::cout<<"modifier_hint():"<< single(p_==tmp2)<<std::endl;
    }

    {
        std::cout<<"Test add:"<<std::endl;
        Profil res =  Profil({{0, Profil::Block(0,1.0,1.0)},
                               {1, Profil::Block(1,0.25,2)},
                               {1.25, Profil::Block(1.25,0.1,3)},
                               {1.35, Profil::Block(1.35,0.15,2)},
                               {1.5, Profil::Block(1.5,0.5,1)},
                               {2, Profil::Block(2,1,2)},
                               {3, Profil::Block(3,1,0)},
                               {4, Profil::Block(4,0.5,1)},
                               {4.5, Profil::Block(4.5,0.5,2)},
                               {5, Profil::Block(5,0.5,3)},
                               {5.5, Profil::Block(5.5,0.5,2)},
                               {6, Profil::Block(6,0.5,3)},
                               {6.5, Profil::Block(6.5,0.5,2)},
                              });
        Profil tmp =   Profil({{1.25, Profil::Block(1.25,0.1,1.0)},
                               {4.5, Profil::Block(4.5,2,1)},
                              });
        Profil p_(d.profil);

        p_.add(tmp);
        std::cout<<"add():"<< single(p_==res)<<std::endl;
        Profil p_2(d.profil);
        p_2.modifier(tmp, [](const Profil::Block & x, const Profil::Block & y) {return x.val+y.val;});
        std::cout<<"modifier(, [](){+}):"<< single(p_2==res)<<std::endl;
    }
    {
      Profil res =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.25,2)},
                              {1.25, Profil::Block(1.25,0.1,1)},
                              {1.35, Profil::Block(1.35,0.15,2)},
                              {1.5, Profil::Block(1.5,0.5,1)},
                              {2, Profil::Block(2,1,2)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,0.5,1)},
                              {4.5, Profil::Block(4.5,0.5,0.5)},
                              {5, Profil::Block(5,0.5,1)},
                              {5.5, Profil::Block(5.5,0.5,0.5)},
                              {6, Profil::Block(6,0.5,1)},
                              {6.5, Profil::Block(6.5,0.5,2)},
                             });
      Profil tmp =   Profil({{1.25, Profil::Block(1.25,0.1,2)},
                              {4.5, Profil::Block(4.5,2,2)},
                             });
      Profil p_(d.profil);
      p_.modifier(tmp, [](const Profil::Block & x, const Profil::Block & y) {return x.val/y.val;});
      std::cout<<"modifier(, [](){/}):"<< single(p_==res)<<std::endl;
    }

    {

        Cover C("test/aff_test1.txt", L);
        Profil p(C.group(0));
        Profil p2(-5,15,3);
        p.modifier(p2, [](const Profil::Block & x, const Profil::Block & y) {return x.val/y.val;});
        Profil res =  Profil({{4, Profil::Block(4,1,1.0/3.0)},
                              {5, Profil::Block(5,0.5,2.0/3.0)},
                              {5.5, Profil::Block(5.5,0.5,1.0/3.0)},
                              {6, Profil::Block(6,1,2.0/3.0)},
                          });
        std::cout<<"modifier(, [](){/})_bis:"<< single(p==res)<<std::endl;
    }
    std::cout<<"Test AUC:"<<std::endl;
    std::cout<<"AUC[0;7]:"<< single(d.profil.AUC(0,7) == 9)<<std::endl;
    std::cout<<"AUC[-3;42]:"<< single(d.profil.AUC(-3,42) == 9)<<std::endl;
    std::cout<<"AUC[-3;0.5]:"<< single(d.profil.AUC(-3,0.5) == 0.5)<<std::endl;
    std::cout<<"AUC[0.25;0.5]:"<< single(d.profil.AUC(0.25,0.5) == 0.25)<<std::endl;
    std::cout<<"AUC[0.25;1.25]:"<< single(d.profil.AUC(0.25,1.25) == 1.25)<<std::endl;
    std::cout<<"AUC[1;1.5]:"<< single(d.profil.AUC(1,1.5) == 1)<<std::endl;
    std::cout<<std::endl;

    std::cout<<"Test between:"<<std::endl;
    std::cout<<"between[0;7]:"<< single(d.between(0,7) == density(7,9,4))<<std::endl;
    std::cout<<"between[-3;42]:"<< single(d.between(-3,42) == density(45,9,4))<<std::endl;
    std::cout<<"between[-3;0.5]:"<< single(d.between(-3,0.5) == density(3.5,0.5,4))<<std::endl;
    std::cout<<"between[0.25;0.5]:"<< single(d.between(0.25,0.5) == density(0.25,0.25,4))<<std::endl;
    std::cout<<"between[0.25;1.25]:"<< single(d.between(0.25,1.25) == density(1,1.25,4))<<std::endl;
    std::cout<<"between[1;1.5]:"<< single(d.between(1,1.5) == density(0.5,1,4))<<std::endl;
    std::cout<<"between[10;12]:"<< single(d.between(10,12) == density(2,0,4))<<std::endl;
    std::cout<<std::endl;

    // DensityAnalyser(const linkStream & L_, const Group &g1, const Group &g2);
    {
        std::cout<<"Test variableDuration:"<<std::endl;
        Profil res =  Profil({{2, Profil::Block(2,1,d.between(2,4))},
                               {3, Profil::Block(3,0.5,d.between(2,5))},
                               {3.5, Profil::Block(3.5,0.5,d.between(2,5.5))},
                               {4, Profil::Block(4,1,d.between(2,6))},
                               {5, Profil::Block(5,0,d.between(2,7))},
                              });
        Profil tmp = d.variableDuration(2,5,2);
        std::cout<<"variableDuration(2,5,2):"<< single(tmp==res)<<std::endl;

        res =  Profil({{2, Profil::Block(2,0.5,d.between(2.5,4.5))},
                               {2.5, Profil::Block(2.5,0.5,d.between(2.5,5))},
                               {3, Profil::Block(3,0.5,d.between(2.5,5.5))},
                               {3.5, Profil::Block(3.5,0.5,d.between(2.5,6))},
                               {4, Profil::Block(4,0,d.between(2.5,6.5))},
                              });
        tmp = d.variableDuration(2, 4, 2.5);

        res =  Profil({{2, Profil::Block(2,1,d.between(-3,-1))},
                               {3, Profil::Block(3,1,d.between(-3,0))},
                               {4, Profil::Block(4,0.5,d.between(-3,1))},
                               {4.5, Profil::Block(4.5,0.5,d.between(-3,1.5))},
                               {5, Profil::Block(5,0,d.between(-3,2))},
                              });
        tmp = d.variableDuration(2, 5, -3);
        std::cout<<"variableDuration(2,5,-3):"<< single(tmp==res)<<std::endl;

        res =  Profil({{2, Profil::Block(2,1,d.between(2,4))},
                       {3, Profil::Block(3,0.5,d.between(2,5))},
                       {3.5, Profil::Block(3.5,0.5,d.between(2,5.5))},
                       {4, Profil::Block(4,1,d.between(2,6))},
                       {5, Profil::Block(5,45,d.between(2,7))},
                       {50, Profil::Block(50,0,d.between(2,52))},
                      });
        tmp = d.variableDuration(2, 50, 2);
        std::cout<<"variableDuration(2,50,2):"<< single(tmp==res)<<std::endl;

        res =  Profil({{0.1, Profil::Block(0.1,0.5,d.between(2.1,2.2))},
                       {0.6, Profil::Block(0.6,0,d.between(2.1,2.7))},
                      });
        tmp = d.variableDuration(0.1, 0.6, 2.1);
        std::cout<<"variableDuration(0.1,0.6,2.1):"<< single(tmp==res)<<std::endl;
        std::cout<<std::endl;
    }
    {
        std::cout<<"Test variableStart:"<<std::endl;
        Profil res =  Profil({{1.25, Profil::Block(1.25,0.25,d.between(1.25,3.25))},
                               {1.5, Profil::Block(1.5,0.5,d.between(1.5,3.5))},
                               {2, Profil::Block(2,1,d.between(2,4))},
                               {3, Profil::Block(3,0.5,d.between(3,5))},
                               {3.5, Profil::Block(3.5,0.3,d.between(3.5,5.5))},
                               {3.8, Profil::Block(3.8,0,d.between(3.8,5.8))},
                              });
        auto tmp = d.variableStart(1.25, 3.8, 2);
        std::cout<<"variableStart(1.25,3.8,2):"<< single(tmp==res)<<std::endl;

        std::cout<<std::endl;
        Profil res2 =  Profil({{-3, Profil::Block(-3,1,d.between(-3,-1))},
                               {-2, Profil::Block(-2,1,d.between(-2,0))},
                               {-1, Profil::Block(-1,0.5,d.between(-1,1))},
                               {-0.5, Profil::Block(-0.5,0.5,d.between(-0.5,1.5))},
                               {0, Profil::Block(0,1,d.between(0,2))},
                               {1, Profil::Block(1,0.5,d.between(1,3))},
                               {1.5, Profil::Block(1.5,0.5,d.between(1.5,3.5))},
                               {2, Profil::Block(2,1,d.between(2,4))},
                               {3, Profil::Block(3,0.5,d.between(3,5))},
                               {3.5, Profil::Block(3.5,0.3,d.between(3.5,5.5))},
                               {3.8, Profil::Block(3.8,0,d.between(3.8,5.8))},
                              });
        tmp = d.variableStart(-3, 3.8, 2);
        std::cout<<"variableStart(-3,3.8,2):"<< single(tmp==res2)<<std::endl;


        res =  Profil({{-3, Profil::Block(-3,1,d.between(-3,-1))},
                               {-2, Profil::Block(-2,1,d.between(-2,0))},
                               {-1, Profil::Block(-1,0.5,d.between(-1,1))},
                               {-0.5, Profil::Block(-0.5,0.5,d.between(-0.5,1.5))},
                               {0, Profil::Block(0,1,d.between(0,2))},
                               {1, Profil::Block(1,0.5,d.between(1,3))},
                               {1.5, Profil::Block(1.5,0.5,d.between(1.5,3.5))},
                               {2, Profil::Block(2,1,d.between(2,4))},
                               {3, Profil::Block(3,0.5,d.between(3,5))},
                               {3.5, Profil::Block(3.5,0.5,d.between(3.5,5.5))},
                               {4, Profil::Block(4,1,d.between(4,6))},
                               {5, Profil::Block(5,0.5,d.between(5,7))},
                               {5.5, Profil::Block(5.5,0,d.between(5.5,7.5))},
                              });
        tmp = d.variableStart(-3, 5.5, 2);
        std::cout<<"variableStart(-3,5.5,2):"<< single(tmp==res)<<std::endl;


        res =  Profil({{-3, Profil::Block(-3,1,d.between(-3,-1))},
                               {-2, Profil::Block(-2,1,d.between(-2,0))},
                               {-1, Profil::Block(-1,0.5,d.between(-1,1))},
                               {-0.5, Profil::Block(-0.5,0.5,d.between(-0.5,1.5))},
                               {0, Profil::Block(0,1,d.between(0,2))},
                               {1, Profil::Block(1,0.5,d.between(1,3))},
                               {1.5, Profil::Block(1.5,0.5,d.between(1.5,3.5))},
                               {2, Profil::Block(2,1,d.between(2,4))},
                               {3, Profil::Block(3,0.5,d.between(3,5))},
                               {3.5, Profil::Block(3.5,0.5,d.between(3.5,5.5))},
                               {4, Profil::Block(4,1,d.between(4,6))},
                               {5, Profil::Block(5,0.5,d.between(5,7))},
                               {5.5, Profil::Block(5.5,0.5,d.between(5.5,7.5))},
                               {6, Profil::Block(6,1,d.between(6,8))},
                               {7, Profil::Block(7,3,d.between(7,9))},
                               {10, Profil::Block(10,0,d.between(10,12))},
                              });
        tmp = d.variableStart(-3,10,2);
        std::cout<<"variableStart(-3,10,2):"<< single(tmp==res)<<std::endl;


        res =  Profil({{-1, Profil::Block(-1,1,d.between(-1,11))},
                        {0, Profil::Block(0,1,d.between(0,12))},
                        {1, Profil::Block(1,0.5,d.between(1,13))},
                        {1.5, Profil::Block(1.5,0.5,d.between(1.5,13.5))},
                        {2, Profil::Block(2,1,d.between(2,14))},
                        {3, Profil::Block(3,1,d.between(3,15))},
                        {4, Profil::Block(4,1,d.between(4,16))},
                        {5, Profil::Block(5,0.5,d.between(5,17))},
                        {5.5, Profil::Block(5.5,0.5,d.between(5.5,17.5))},
                        {6, Profil::Block(6,1,d.between(6,18))},
                        {7, Profil::Block(7,3,d.between(7,19))},
                        {10, Profil::Block(10,0,d.between(10,22))},
                              });
        tmp = d.variableStart(-1,10,12);
        std::cout<<"variableStart(-1,10,12):"<< single(tmp==res)<<std::endl;


        res =  Profil({{2.5, Profil::Block(2.5,0.5,d.between(2.5,14.5))},
                        {3, Profil::Block(3,1,d.between(3,15))},
                        {4, Profil::Block(4,1,d.between(4,16))},
                        {5, Profil::Block(5,0.5,d.between(5,17))},
                        {5.5, Profil::Block(5.5,0.5,d.between(5.5,17.5))},
                        {6, Profil::Block(6,1,d.between(6,18))},
                        {7, Profil::Block(7,3,d.between(7,19))},
                        {10, Profil::Block(10,0,d.between(10,22))},
                              });
        tmp = d.variableStart(2.5,10,12);
        std::cout<<"variableStart(2.5,10,12):"<< single(tmp==res)<<std::endl;

        res =  Profil({{2, Profil::Block(2,1,d.between(2,14))},
                        {3, Profil::Block(3,1,d.between(3,15))},
                        {4, Profil::Block(4,1,d.between(4,16))},
                        {5, Profil::Block(5,0.5,d.between(5,17))},
                        {5.5, Profil::Block(5.5,0.5,d.between(5.5,17.5))},
                        {6, Profil::Block(6,1,d.between(6,18))},
                        {7, Profil::Block(7,3,d.between(7,19))},
                        {10, Profil::Block(10,0,d.between(10,22))},
                              });
        tmp = d.variableStart(2,10,12);
        std::cout<<"variableStart(2,10,12):"<< single(tmp==res)<<std::endl;

        res =  Profil({{7, Profil::Block(7,3,d.between(7,19))},
                       {10, Profil::Block(10,0,d.between(10,22))},
                              });
        tmp = d.variableStart(7,10,12);
        std::cout<<"variableStart(7,10,12):"<< single(tmp==res)<<std::endl;
        std::cout<<std::endl;
    }
    {
        Profil prof =  Profil({{1353303380, Profil::Block(1353303380,20,1)},
                               {1353303400, Profil::Block(1353303400,880,1)},
                               {1353304280, Profil::Block(1353304280,60,1)},
                              });
        DensityAnalyser anlyser(prof, {1,2});
        Profil res =  Profil({{1353303360, Profil::Block(1353303360,20,920.0/940.0)},
                               {1353303380, Profil::Block(1353303380,20,1.0)},
                               {1353303400, Profil::Block(1353303400,880,1.0)},
                               {1353304280, Profil::Block(1353304280,60,60.0/940)},
                               {1353304340, Profil::Block(1353304340,728540,0)},
                               {1354032880, Profil::Block(1354032880,0,0)},
                              });
        Profil tmp = anlyser.variableStart(1353303360, 1354032880, 940);
        std::cout<<"variableStart(1353303360,1354032880,940):"<< single(tmp==res)<<std::endl;
    }

    {
        Profil prof =  Profil({{1353304000, Profil::Block(1353304000,20,1)}
                              });
        DensityAnalyser anlyser(prof, {1,2});
        Profil res =  Profil({{1353303360, Profil::Block(1353303360,620,0)},
                               {1353303980, Profil::Block(1353303980,20,0)},
                               {1353304000, Profil::Block(1353304000,20,1)},
                               {1353304020, Profil::Block(1353304020,728840,0)},
                               {1354032860, Profil::Block(1354032860,0,0)},
                              });
        Profil tmp = anlyser.variableStart(1353303360, 1354032860, 20);
        std::cout<<"variableStart(1353303360,1354032860,20):"<< single(tmp==res)<<std::endl;
    }
    {
        Profil prof =  Profil({{1353304000, Profil::Block(1353304000,20,1)}
                              });
        DensityAnalyser anlyser(prof, {1,2});
        Profil res =  Profil({{1353304005, Profil::Block(1353304005,5,0.5)},
                               {1353304010, Profil::Block(1353304010,0,1.0/3.0)},
                              });
        Profil tmp = anlyser.variableStart(1353304005, 1353304010, 30);
        std::cout<<"variableStart(1353304005,1353304010,30):"<< single(tmp==res)<<std::endl;
    }
    {
        Profil prof =  Profil({{50, Profil::Block(50,1,1)}
                              });
        DensityAnalyser anlyser(prof, {1,2});
        std::cout<<"Test variableDuration:"<<std::endl;
        Profil res =  Profil({{2, Profil::Block(2,8,0)},
                               {10, Profil::Block(10,1,0)},
                               {11, Profil::Block(11,9,1.0/11.0)},
                               {20, Profil::Block(20,0,1.0/20.0)},
                              });
        Profil tmp = anlyser.variableDuration(2,20,40);
        std::cout<<"variableDuration(2,5,2):"<< single(tmp==res)<<std::endl;
    }

    {
        std::cout<<"Test percentil_pointwise:"<<std::endl;
        Profil tmp=Profil({{0, Profil::Block(0,1,0.1)},
                           {1, Profil::Block(1,2,0.2)},
                           {3, Profil::Block(3,1,0.8)},
                           {4, Profil::Block(4,1,0.8)},
                           {5, Profil::Block(5,1,0.2)},
                           {6, Profil::Block(6,0.5,0.6)},
                           {6.5, Profil::Block(6.5,0,0.4)},
                       });
        std::cout<<"percentil_pointwise(2):"<< single(tmp.percentil_pointwise(2) == 1)<<std::endl;
        std::cout<<"percentil_pointwise(-4):"<< single(tmp.percentil_pointwise(-4) == 0)<<std::endl;
        std::cout<<"percentil_pointwise(0.2):"<< single(std::abs(tmp.percentil_pointwise(0.2)-1/6.5)<0.00000001)<<std::endl;
        std::cout<<"percentil_pointwise(0.5):"<< single(std::abs(tmp.percentil_pointwise(0.5)-3.5/6.5)<0.00000001)<<std::endl;
        std::cout<<"percentil_pointwise(0.8):"<< single(std::abs(tmp.percentil_pointwise(0.8)-5.5/6.5)<0.00000001)<<std::endl;
        std::cout<<"percentil_pointwise(2):"<< single(tmp.percentil_pointwise(2) == 1)<<std::endl;


        std::cout<<"percentil_stepFunction(-4):"<< single(tmp.percentil_stepFunction(-4) == 0)<<std::endl;
        std::cout<<"percentil_stepFunction(0.2):"<< single(std::abs(tmp.percentil_stepFunction(0.2)-1/6.5)<0.00000001)<<std::endl;
        std::cout<<"percentil_stepFunction(0.5):"<< single(std::abs(tmp.percentil_stepFunction(0.5)-4/6.5)<0.00000001)<<std::endl;
        std::cout<<"percentil_stepFunction(0.8):"<< single(std::abs(tmp.percentil_stepFunction(0.8)-4.5/6.5)<0.00000001)<<std::endl;
        std::cout<<"percentil_stepFunction(0.8):"<< single(std::abs(tmp.percentil_stepFunction(0.9)-1)<0.00000001)<<std::endl;
        std::cout<<std::endl;
    }
    return true;
}
