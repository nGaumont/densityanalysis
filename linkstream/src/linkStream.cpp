#include "linkStream.hpp"

using namespace gaumont;

std::ostream &gaumont::operator<<(std::ostream &os, const Link &l) {
  const char *sep = " ";
  os << l.t << sep << l.left() << sep << l.right() << sep << l.duration;
  return os;
}

std::ostream &gaumont::operator<<(std::ostream &os, const Node &n) {
  os << n.id();
  return os;
}


void tmpLink::operator=(const Link * l){
    id=l->id;
    t=l->t;
    u=l->left().id();
    v=l->right().id();
    duration = l->duration;
}

void Node::add(Link &l) { linkNeighbor.push_back(&l); }

void Node::displayLinkNeighbor(std::string prefix) const {
  std::deque<Link *>::const_iterator it = linkNeighbor.begin(),
                                     ite = linkNeighbor.end();
  std::cout << prefix << ": " << std::endl;
  for (; it != ite; ++it) {
    std::cout << "\t" << *(*it) << std::endl;
  }
}
double Link::intersection(const Link &l) const {
  double dif = std::min(l.t + l.duration, t + duration) - std::max(t, l.t);
  // std::cout<<*this<<" et "<< l<<std::endl;
  return dif;
}
double Link::intersection(const tmpLink & l) const{
    double dif = std::min(l.t + l.duration, t + duration) - std::max(t, l.t);
    return dif;
}

bool linkComp(const Link *a, const Link *b) { return a->id < b->id; }

void Link::linkNeighbor(std::deque<Link *> &res) const {
  res.clear();
  const std::deque<Link *> &left = this->left().linksNeighbor();
  const std::deque<Link *> &right = this->right().linksNeighbor();
  res.resize(left.size() +
             right.size()); // make sur res is large enough to store everything.

  std::deque<Link *>::iterator ite;
  ite = std::set_union(left.begin(), left.end(), right.begin(), right.end(),
                       res.begin(), linkComp); // fill res with the union
  // ite points to the end of added values
  res.resize(ite - res.begin()); // As doublon could occurs=> ite!= res.end().
                                 // Therefore res has to be resized.
                                 // The current link is in the dequeu;
}

std::pair<bool, nodeID> Link::commonNode(const Link &l) const {
  std::pair<bool, nodeID> res(false, nodeID(0));
  if (left().id() == l.left().id() || left().id() == l.right().id()) {
    res.first = true;
    res.second = left().id();
  } else if (right().id() == l.left().id() || right().id() == l.right().id()) {
    res.first = true;
    res.second = right().id();
  }
  return res;
}
