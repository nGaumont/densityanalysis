#ifndef TESTMASTER_H
#define TESTMASTER_H
#include <iostream>


#include "linkStream.hpp"
#include "community.hpp"
#include "densityAnalyser.hpp"
#include "profil.hpp"


namespace gaumont {

class TestMaster{
    unsigned int fail;
    unsigned int total;
    std::string single(bool b){
        ++this->total;
        if(!b){
            ++this->fail;
            return "fail!";
        }
        return "ok.";
    }
public:
    TestMaster():fail(0), total(0){};
    bool testLinkStream();
    bool testCommunity();
    bool testDensityAnalyser();
    bool testProfil();

    bool passed()const{
        return fail==0;
    }
    void all(){
        testLinkStream();
        testCommunity();
        testDensityAnalyser();
        testProfil();
        std::cout<<"---- Final result -----"<<std::endl;
        std::cout<<this->total-this->fail<<"/"<<this->total<<std::endl;
    }
};


}
#endif
