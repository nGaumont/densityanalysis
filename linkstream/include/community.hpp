#ifndef COMMUNITY_H
#define COMMUNITY_H

#include "linkStream.hpp"

namespace gaumont {

class Cover;


class Group {
  typedef std::set<const Link *, linkComparator > linkSet;

  Group::linkSet link_set;
  mutable std::set<const Node *, nodeComparator > node_set;
  mutable bool updateNodeset;
  unsigned int identifier;
  friend class Cover;


  bool add_hint(linkSet::iterator hint, const Link & l);

  //Group(const Group & G):link_set(G.link_set),node_set(),updateNodeset(false), identifier(G.identifier), C(G.C){}
 public:
    typedef linkSet::const_iterator const_iterator;
    const_iterator begin() const{return link_set.begin();}
    const_iterator end() const{return link_set.end();}

    //TODO make a two comparator: adresse comparator and content comparator.
    bool operator==(const Group & g)const{return this == &g;}
    Group():link_set(), node_set(), updateNodeset(false), identifier(){}

    //Return the id of the group.
    unsigned int id()const{return identifier;}

    // return True if the link has been added to the group
    bool add(const Link & l);
    // return True if the link has been remove from the group
    // The group might be empty after this.
    bool remove(const Link & l);

    // give a view on the set of nodes which are at least connected to on link of the group.
    const std::set<const Node *, nodeComparator> &  inducedNode()const;

    // Return the time span of the group: first link appareance and last link extinction.
    timespan lifeTime()const;

    // return True if the link given in parameter is in the group.
    bool contains(const Link & )const;

    // return the number of links in the group
    unsigned int size()const{return link_set.size();}


    void dump()const{
        for(auto link: link_set){
            std::cout<<*link<<std::endl;
        }
    }
};

std::ostream &operator<<(std::ostream &out, const Group &g);


class Cover {
    std::map<unsigned int, Group> com_struct;
    std::map<linkID, unsigned int> assignation;

    Group & groupContaining(const Link & l);
public:

    //Class that let the user iterate on groups.
    class const_iterator{
        friend class Cover;
        typename std::map<unsigned int, Group>::const_iterator pointer;
        const_iterator(typename std::map<unsigned int, Group>::const_iterator it):pointer(it){}
        const_iterator();
    public:
        const_iterator(const const_iterator& it): pointer(it.pointer){};
        bool operator==(const const_iterator& it) const{return this->pointer == it.pointer;}
        bool operator!=(const const_iterator& it) const{return !(*this==it);}
        const_iterator& operator++(){++pointer; return *this;} //prefix increment
        const Group& operator*() const{return pointer->second;}
    };
    Cover::const_iterator begin() const{return const_iterator(com_struct.begin());}
    Cover::const_iterator end() const{return const_iterator(com_struct.end());}

    // Input File is affiliation file, i.e. list of link_id comID,
    // L is the linkstream that inputFile should partition.
    template<LinkType LT>
    Cover(const char *inputFile, const linkStream<LT> & L);

    // Create a partition where each link is in its own group.
    template<LinkType LT>
    Cover(const linkStream<LT> & L);

    // Return a const ref to the group which have a given id(panic if not existing)
    const Group & group(unsigned int gID)const;

    // return a ref the group which contains the link given in parameter.
    const Group & containing(const Link &)const;

    //return the number of grops in the cover.
    unsigned int size()const;

    // Check that each link has a group.
    template<LinkType LT>
    bool checkCoverage(const linkStream<LT> & L)const;

    // move a link to the group which has a given id.
    //TODO change id to ref to the group.
    void moveInto(const Link & , unsigned int);

    // write the affiliation of each link in the given ostream.
    void dumpAffiliation(std::ostream &)const;
};
inline std::ostream &operator<<(std::ostream &os, const Cover & C){
    C.dumpAffiliation(os);
    return os;
}

#include "community.tpp"
}// namespace end
#endif
