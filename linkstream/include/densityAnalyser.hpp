#ifndef DENSITYANALYSER_H
#define DENSITYANALYSER_H
#include "linkStream.hpp"
#include "community.hpp"
#include "profil.hpp"
#include <iomanip>
#include <limits>       // std::numeric_limits
#include <cmath> // std::isnormal


namespace gaumont {





// This class gather tools to analyse the density of a given groups of nodes.
// Assume undirected link, no self loopand simple linkstream.
class DensityAnalyser{
     friend class TestMaster;
    std::set<nodeID > nodes; // The nodes of interest

    //This class holds the underlying structure to evalute a group.

    Profil profil; //store at each timestamp the number of active  link.
    //Prevent default
    DensityAnalyser();
    DensityAnalyser(const DensityAnalyser &d); //:nodes(d.nodes),profil(d.profil){}
    explicit DensityAnalyser(const Profil & p, const std::set<nodeID> & nodes_): nodes(nodes_), profil(p){}


    class closeNode{
    private:
        std::set<nodeID> base;
        std::set<nodeID> current;
        std::set<nodeID> remaining;

        std::set<nodeID>::iterator base_it;
        std::set<nodeID>::iterator remaining_it;

      public:
         closeNode(const std::set<nodeID> & Nodes,const linkStream<Duration> &L): base(Nodes), current(Nodes), remaining(){
            linkStream<Duration>::const_NodesIterator it=L.beginNodes(), ite=L.endNodes();
            for(;it!=ite;++it){
               remaining.insert((*it).first);
            }
            for(auto node: base){
               remaining.erase(node);
            }
            base_it= base.begin();
            remaining_it= remaining.begin();

            current.erase(*base_it);
            current.insert(*remaining_it);
         }

         bool operator++(){
            if(empty()){
              current.clear();
              return false;
            }
            current.erase(*remaining_it);
            ++remaining_it;

            if(remaining_it==remaining.end()){
               remaining_it = remaining.begin();
               current.insert(*base_it);
               ++base_it;
               if(base_it == base.end()){
                 current.clear();
                 return false;
              }
              current.erase(*base_it);
            }
            current.insert(*remaining_it);
            return true;
         }

         const std::set<nodeID> & operator*()const{return current;}

         void print()const{
            std::cout<<"{";
            for(auto node: current){
               std::cout<<node<<", ";
            }
            std::cout<<"}"<<std::endl;
         }
         bool empty()const{
            return remaining_it==remaining.end() || base_it == base.end();
         }
    };




public:
    template<LinkType LT>
    explicit DensityAnalyser(const linkStream<LT> & L, const std::set<const Node *, nodeComparator > & nodes_, bool incluse, bool assume_simple):nodes(), profil(L, nodes_, incluse, assume_simple){
        for(auto n: nodes_){
            nodes.insert(nodes.end(),(*n).id());
        }
    }
    template<LinkType LT>
    explicit DensityAnalyser(const linkStream<LT> & L, const std::set<nodeID> & nodes_, bool incluse , bool assume_simple) : nodes(nodes_), profil(L, nodes, incluse, assume_simple){}

    template<LinkType LT>
    explicit DensityAnalyser(const linkStream<LT> & L, const std::vector<nodeID> & nodes_, bool incluse , bool assume_simple) : nodes(nodes_.begin(),nodes_.end()), profil(L, nodes, incluse, assume_simple){}

    template<LinkType LT>
    explicit DensityAnalyser(const linkStream<LT> & L, const Group &g1, const Group &g2, bool assume_simple):nodes(),profil(){
        for(auto n: g1.inducedNode()){
            nodes.insert(nodes.end(),(*n).id());
        }
        std::set<nodeID >::iterator hint=nodes.begin();
        for(auto n: g2.inducedNode()){
            hint = nodes.insert(hint, (*n).id());
        }
        profil = Profil::inter(L, g1, g2, nodes, assume_simple);
    }
    template<LinkType LT>
    explicit DensityAnalyser(const linkStream<LT> & L,  bool assume_simple):nodes(),profil(L, assume_simple){
        for(typename linkStream<LT>::const_NodesIterator it=L.beginNodes();it !=L.endNodes();++it){
            nodes.insert((*it).first);
        }
    }
    explicit DensityAnalyser(const Group &g,  bool assume_simple):nodes(),profil(g, assume_simple){
        for(auto n: g.inducedNode()){
            nodes.insert(nodes.end(),(*n).id());
        }
    }

    // compute the density if we consider the time interval [beg, end]
    long double between(timestamp, timestamp)const;

    // Compute the Profil of the density if we consider that startTime is  in the interval [beg, end]
    // and a duration of dur.
    Profil variableStart(timestamp, timestamp, long double)const;

    // Compute the profil when the duration change but not the start time.
    // A block [t,dura, val] === with duration t the group has a density of val.
    // In the duration interval [t; t+dura], the density changes linearly.
    // The coef depends on the nodesize and the degree in the interval but is not
    // needed because we just has to use the next or previous block to compute it.
    Profil variableDuration(long double, long double, timestamp)const;


    long double variableNodes(const linkStream<Duration> &L, long double begin, long double end, bool assume_simple)const{
     closeNode generator(nodes, L);
     unsigned int nb_gen = 0, under = 0;
     long double cur_dens = DensityAnalyser(profil,nodes).between(begin, end);
     while(!generator.empty()){
       DensityAnalyser analyser(L, *generator, true, assume_simple);
       long double rand_dens = analyser.between(begin, end);
       ++generator;
       std::cout<<rand_dens<<std::endl;
       if(rand_dens < cur_dens){
           ++under;
       }
       ++nb_gen;
     }
     long double res =(1.0*under)/nb_gen;
     return res;
    }

    unsigned int profilSize()const{return profil.size();}

    void print(std::ostream & os=std::cout)const{os<<profil<<std::endl;}

};
inline std::ostream &operator<<(std::ostream & os, const DensityAnalyser & d){
    d.print(os);
    return os;
}
}
#endif
