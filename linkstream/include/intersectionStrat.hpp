#ifndef INTERSECTIONSTRAT_H
#define INTERSECTIONSTRAT_H

#include "linkStream.hpp"
#include <string>
namespace gaumont{
  // Compute the intersection between 2 links. the order matter. If inter()>0 then
  // There could be a link betwwen l1 and l2.
  class Inter_Strat{
  public:
    const std::string name;
    Inter_Strat(const std::string & s): name(s){}
    virtual long double inter(const Link &,const Link &)=0;
  };

  class PermissiveStrat: public Inter_Strat{
  public:
    PermissiveStrat():Inter_Strat("PermissiveStrat (aka red)"){}
    long double inter(const Link & l1,const Link & l2){
      // CommonNode
      if(l1.commonNode(l2).first)
        return l1.intersection(l2); // return time overlap
      return -1;
    };
  };


  class MiddleStrat: public Inter_Strat{
  public:
    MiddleStrat():Inter_Strat("MiddleStrat (aka green)"){}
    long double inter(const Link & l1,const Link & l2){
      // A common node and not the same sender.
      if(l1.commonNode(l2).first && l1.left().id() != l2.left().id())
        return l1.intersection(l2); // return time overlap.
      return -1;
    };
  };


  class StrictClass: public Inter_Strat{
  public:
    static constexpr const char * name="StrictStrat (aka blue)";
    StrictClass():Inter_Strat("StrictClass (aka blue)"){}
    long double inter(const Link & l1,const Link & l2){
      // Same recipient and time overlap.
      if(l1.right().id() == l2.right().id())
        return l1.intersection(l2); // return time overlap
      return -1;
    };
  };

  class StratFactory{
  public:
    static Inter_Strat* make(const std::string & name){
      std::string NAME = name;
      std::transform(NAME.begin(), NAME.end(), NAME.begin(), toupper);
      Inter_Strat * strat=NULL;
      if(NAME=="1"){
        strat=new PermissiveStrat();
      }else if(NAME=="2"){
        strat=new MiddleStrat();
      }else if(NAME=="3"){
        strat=new StrictClass();
      }
      if (strat !=NULL)
        return strat;
      std::cerr<<"Unknow strat identifier :"<<name<<std::endl;
      exit(-1);
    }
  };

}


#endif
