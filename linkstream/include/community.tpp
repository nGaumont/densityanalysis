

template <LinkType LT>
inline Cover::Cover(const char *inputFile, const linkStream<LT> & L): com_struct(), assignation(){
    std::ifstream finput(inputFile);
    if (!finput) {
      std::cerr << "The file " << inputFile << " does not exist" << std::endl;
      exit(-1);
    }
    std::string line;
    linkID cur_link;
    unsigned int cur_com;
    while (std::getline(finput, line)) {
      std::istringstream iss(line);
      if (!(iss >> cur_link >> cur_com)) {
        std::cerr << "something wrong with:" << std::endl << line << std::endl;
        exit(-1);
      }
      if(cur_link>= L.getNbLinks()){
          std::cerr<<"Can't find linID: "<<cur_link<<std::endl;
          exit(-1);
      }
      // Check if the cur_com already exist or not.
      auto it = com_struct.find(cur_com);
      if (it == com_struct.end()) {
          Group & g = com_struct[cur_com];
          g.identifier=cur_com;
          g.add_hint(g.link_set.end(),L.link(cur_link));
      } else {
        it->second.add_hint(it->second.link_set.end(), L.link(cur_link));
      }
      assignation[cur_link] = cur_com;
    }
    finput.close();
}

template <LinkType LT>
inline Cover::Cover(const linkStream<LT> & L): com_struct(), assignation(){
    for(auto it = L.beginLinks(); it !=L.endLinks();++it){
        linkID cur = (*it).id;
        Group & g = com_struct[cur];
        g.identifier=cur;
        g.add((*it)); // creation of the node
        assignation[cur] = cur;
    }
}

template <LinkType LT>
inline bool Cover::checkCoverage(const linkStream<LT> & L)const{
    auto it = L.beginLinks(), ite =L.endLinks();
    // check that every link in L has a com assigned to it.
    for(; it != ite; ++it){
        if(assignation.find((*it).id) ==  assignation.end()){
            return false;
        }
    }
    // every link in the cover is sur to be in L because of the constructor.
    return true;
}
