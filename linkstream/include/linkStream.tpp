template <LinkType LT>
template<SerializationFormat SF>
inline void linkStream<LT>::readFile(const char *inputFile){
    this->beginning=9999999999999;
    this->ending=-9999999999;
  std::ifstream finput(inputFile);
  if (!finput) {
    std::cerr << "The file " << inputFile << " does not exist" << std::endl;
    exit(-1);
  }

  std::string line;
  timestamp cur = -9999999999999;
  linkID count(0);
  while (std::getline(finput, line)) {
    tmpLink l=tmpLink::create<LT,SF>(line, count);
    if (l.t < cur) {
      std::cerr << "Your file is not time sorted. Line number: " << count << std::endl << line << std::endl;
      exit(-1);
    }
    cur = l.t;
    count++;
    addLink(l);
  }
  finput.close();
}


template <>
template<SerializationFormat SF>
inline void linkStream<Duration>::readFile(const char *inputFile, double d_duration){
    this->beginning=9999999999999;
    this->ending=-9999999999;
  std::ifstream finput(inputFile);
  if (!finput) {
    std::cerr << "The file " << inputFile << " does not exist" << std::endl;
    exit(-1);
  }
  if(SF == SerializationFormat::TUV){
      assert(d_duration>=0);
      std::cerr << "Depreciated combination (Link stream with duration from TUV files). The result is not correct." << std::endl;
  }

  std::string line;
  timestamp cur = -9999999999999;
  linkID count(0);
  while (std::getline(finput, line)) {
    tmpLink l=tmpLink::create<Duration,SF>(line, count, d_duration);
    if (l.t < cur) {
      std::cerr << "Your file is not time sorted. Line number: " << count << std::endl << line << std::endl;
      exit(-1);
    }
    cur = l.t;
    count++;
    addLink(l);
  }
  finput.close();
}
template <LinkType LT>
template<SerializationFormat SF>
inline void linkStream<LT>::readFile(const char *inputFile, double d_duration){
    std::cerr<<"readFile not written for Duration with default duration"<<std::endl;
    std::cerr<<"Parmeters:"<<inputFile<<" "<<d_duration <<std::endl;
    exit(-1);
}


template <LinkType LT>
inline void linkStream<LT>::addLink(const tmpLink &l) {
  // Create node if it doesn't exist yet;
  Node &left = getCreateNode(l.u);
  Node &right = getCreateNode(l.v);
  if (l.t <beginning ){
    beginning = l.t;
  }
  if ( (l.t+l.duration) > ending){
    ending = l.t+l.duration;
  }

  Link realLink(l, left, right);
  links.push_back(realLink); // There is a copy constructor here. That's why we
                             // add the link to the node later.

  left.add(*links.rbegin());
  if (l.u != l.v)
    right.add(*links.rbegin());

  // Update events
  if(left.id() < right.id())
    events[PairLink(left.id(), right.id())].push_back(&(*links.rbegin()));
  else
    events[PairLink(right.id(), left.id())].push_back(&(*links.rbegin()));
}


template <>
template<SerializationFormat SF>
inline bool linkStream<LinkType::Simple>::readBuffer(std::ifstream &input) {
    std::string line;
    if(!std::getline(input, line)){
      return false;
    }
    assert(links.size() <= bufferSize);

    unsigned int last_id= (links.empty())? 0:(*links.rbegin()).id;
    unsigned int count= last_id + 1;
    tmpLink l = tmpLink::create<LinkType::Simple>(line, count);

    timestamp last_t = (links.empty())? MIN_TIMESTAMP:(*links.rbegin()).t;
    if (l.t < last_t) {
      std::cerr << "Your file is not time sorted. Line number: " << count << std::endl << line << std::endl;
      exit(-1);
    }

    // Add link
    addLink(l);

    if(links.size() > bufferSize) {
      Link & to_del= links.front();

      //update Node left
      Node & left = to_del.left();
      assert( left.linkNeighbor.front()->id == to_del.id);
      left.linkNeighbor.pop_front();
      if(left.linkNeighbor.empty()){
        nodes.erase(left.id());
      }
      //Update Node right
      Node & right = to_del.right();
      assert(left.id() != right.id());
      assert( right.linkNeighbor.front()->id == to_del.id);
      right.linkNeighbor.pop_front();
      if(right.linkNeighbor.empty()){
        nodes.erase(right.id());
      }

      //Update pair event
      auto pair = eventList(std::min(left.id(),right.id()), std::max(left.id(),right.id()));
      assert(pair.first);
      assert(pair.second.front()->id == to_del.id);
      pair.second.pop_front();

      links.pop_front();
    }
    beginning = (*links.begin()).t;
    ending = (*links.rbegin()).t;

  return !input.eof();
}

template <LinkType LT>
template<SerializationFormat SF>
inline bool linkStream<LT>::readBuffer(std::ifstream &input) {
  std::cerr << "Undefined." << std::endl;
  return false;
}

template <LinkType LT>
linkStream<LT>::linkStream(unsigned int _bufferSize):beginning (MAX_TIMESTAMP),ending(MIN_TIMESTAMP), bufferSize(_bufferSize) {
}


template <LinkType LT>
inline  bool linkStream<LT>::compLinkList(const linkStream &L)const{
    if (L.links.size() != links.size()){
        std::cout<<"Pas la même taille."<<L.links.size() <<" "<< links.size()<<std::endl;
        return false;
    }
    auto it1=links.begin(), ite1=links.end();
    auto it2=L.links.begin(), ite2=L.links.end();
    
    for(;it1!=ite1 && it2!=ite2; ++it1, ++it2){
        if(!(*it1).weakEqual(*it2)){
            std::cout<<"Pas le même lien. "<<(*it1).id <<" "<<*it1 <<" et "<<(*it2).id <<" "<< *it2<<std::endl;
            return false;
        }
    }
    return true;
}
template <LinkType LT>
inline  bool linkStream<LT>::compLinkList(const std::deque<tmpLink> & tmpLinks)const{
    if (tmpLinks.size() != links.size()){
        std::cout<<"Not the same size: "<<tmpLinks.size() <<" "<< links.size()<<std::endl;
        return false;
    }
    auto it1=links.begin(), ite1=links.end();
    auto it2=tmpLinks.begin(),ite2=tmpLinks.end();
    for(;it1!=ite1 && it2!=ite2; ++it1, ++it2){
        if(!(*it1).weakEqual(*it2)){
            std::cout<<"Pas le même lien. "<<(*it1).id <<" "<<*it1 <<" et "<<(*it2).id <<" "<< *it2<<std::endl;
            return false;
        }
    }
    return true;
}
template <LinkType LT>
inline  std::pair <bool,const std::deque<Link *> & >linkStream<LT>::eventList(nodeID a, nodeID b) const {
  std::map<PairLink, std::deque<Link *> >::const_iterator it =
      events.find(PairLink(a, b));
  if (it == events.end()) {
    return {false,it->second};
  }
  return {true,it->second};
}

template <LinkType LT>
inline  std::pair <bool,std::deque<Link *> & > linkStream<LT>::eventList(nodeID a, nodeID b) {
  std::map<PairLink, std::deque<Link *> >::iterator it =
      events.find(PairLink(a, b));
  if (it == events.end()) {
    return {false,it->second};
  }
  return {true,it->second};
}

template <LinkType LT>
inline  bool linkStream<LT>::isSimple()const{
    for(auto pair: events){
        if(pair.second.size()<2){
            continue;
        }
        std::deque<Link *>::const_iterator it=pair.second.begin(), prev=it,ite=pair.second.end();
        ++it;
        for(;it!=ite;++it){
            if((**it).intersection(**prev)>0){
                return false;
            }
            prev=it;
        }
    }
    return true;
}

template <>
inline  linkStream<LinkType::Duration> linkStream<LinkType::Duration>::makeSimple()const{
    std::deque<tmpLink> links;
    for(auto pair: events){
        if(pair.second.size()==1){
            links.emplace_back(pair.second[0]->makeTmpLink());
            continue;
        }
        std::deque<Link *>::const_iterator it=pair.second.begin(), prev=it,ite=pair.second.end();
        ++it;
        tmpLink prev_tmp= (**prev).makeTmpLink();
        for(;it!=ite;++it){
            if((**it).intersection(**prev)>0){
                //Update prev
                prev_tmp.duration = std::max(prev_tmp.t +prev_tmp.duration, (**it).lifeTime().end)-prev_tmp.t;
            }else{
                //no intersection we can add safely
                links.emplace_back(prev_tmp);
                prev_tmp= tmpLink((**it).makeTmpLink());
            }
            prev=it;

        }
        links.emplace_back(prev_tmp);
    }
    std::sort(links.begin(),links.end(),[](const tmpLink& l1, const tmpLink& l2){
        if (l1.t<l2.t){
            return true;
        }
        if (l1.t==l2.t){
            if (l1.u> l2.u){
                return true;
            }
            if(l1.v> l2.v){
                return true;
            }
        }
        return false;
    });
    //Now we iterate in time ordered fashion the tmplink.
    // This two step process is needed in order to create correct id
    linkStream L;
    unsigned int count =0;
    for(auto tmpLn: links){
        tmpLn.id=count;
        L.addLink(tmpLn);
        ++count;
    }
    return L;
}

template <LinkType LT>
inline linkStream<LT> linkStream<LT>::makeSimple()const{
    std::cerr<<"makeSimple: computation not written"<< std::endl;
    exit(-1);
}

template <LinkType LT>
void linkStream<LT>::printDegreeList(std::ostream &f) const {
  const_NodesIterator it = beginNodes(), ite = endNodes();
  f << it->second.degree();
  ++it;
  for (; it != ite; ++it) {
    f << "," << it->second.degree();
  }
  f << std::endl;
}
