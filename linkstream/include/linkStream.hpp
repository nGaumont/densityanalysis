#ifndef LINKSTREAM_H
#define LINKSTREAM_H

#include <map>
#include <deque>
#include <set>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <assert.h>
#include <stdexcept>
#include <vector>
#include <cmath>

#include <limits>

#include "object.h"

#define  MAX_UINT std::numeric_limits<unsigned int>::max()
#define  MAX_TIMESTAMP std::numeric_limits<timestamp>::max()
#define  MIN_TIMESTAMP std::numeric_limits<timestamp>::min()

namespace gaumont {

class Link;
typedef unsigned int nodeID;
typedef unsigned int linkID;
typedef long double timestamp;

enum SerializationFormat{
    BEUV,
    TUVD,
    TUV,
    UNKNOWN
};
inline std::ostream &operator<<(std::ostream & out, const SerializationFormat & SF){
    std::string res;
    switch(SF){
        case BEUV: res="BEUV"; break;
        case TUV: res="TUV"; break;
        case TUVD: res="TUVD"; break;
        default: res="UNKNOWN"; break;
    }
    out<<res;
    return out;
}


enum LinkType{
    Simple,
    Duration
};
inline std::ostream &operator<<(std::ostream & out, const LinkType & LT){
    std::string res;
    switch(LT){
        case Simple: res="Simple"; break;
        case Duration: res="Duration"; break;
        default: res="UNKNOWN"; break;
    }
    out<<res;
    return out;
}

constexpr SerializationFormat defaultValue(LinkType LT){
    return (LT== LinkType::Simple)? SerializationFormat::TUV :
           (LT==LinkType::Duration)? SerializationFormat::BEUV:
           SerializationFormat::UNKNOWN;
}

template <LinkType LT>
class linkStream;


template <typename T> static std::string toStr(const T &val) {
  std::stringstream out;
  out << val;
  return out.str();
}


typedef std::pair<nodeID, nodeID> PairLink;
struct PairLinkComp {
  bool operator()(const PairLink &l1, const PairLink &l2) const {
    if (l1.first > l2.first) {
      return false;
    }
    if (l1.first == l2.first) {
      return l1.second < l2.second;
    }
    return true;
  }
};

// Should not be puplic
class tmpLink {
public:
  tmpLink(unsigned int id_, timestamp t_, nodeID u_, nodeID v_, double dura_)
      : id(id_), u(u_), v(v_), t(t_), duration(dura_) {}
  tmpLink(const tmpLink &l): id(l.id), u(l.u), v(l.v), t(l.t), duration(l.duration) {}
  tmpLink(): id(MAX_UINT), u(MAX_UINT), v(MAX_UINT), t(-1), duration(-1) {}

  bool empty()const{return id==MAX_UINT && u==MAX_UINT && v==MAX_UINT && t==-1 && duration==-1;}

  timestamp ending()const{return t+duration;}
  void operator=(const Link * l);

  template <LinkType LT, SerializationFormat SF=defaultValue(LT)>
  static tmpLink create(const std::string&, unsigned int, double);

  template <LinkType LT, SerializationFormat SF=defaultValue(LT)>
  static tmpLink create(const std::string&, unsigned int);

  linkID id;
  nodeID u;
  nodeID v;
  timestamp t;
  double duration;
};

template <>
inline tmpLink tmpLink::create<LinkType::Simple, SerializationFormat::TUV>(const std::string& line, unsigned int id){
    std::istringstream iss(line);
    bool readed =false;
    timestamp t;
    nodeID u,v;
    readed = static_cast<bool>(iss >> t >> u >> v);
    if (!(readed)) {
      std::cerr << "There is a problem with the line :" << std::endl << line
                << std::endl;
      exit(-1);
    }
    return tmpLink(id,t,u,v,0.0);
}
template <>
inline tmpLink tmpLink::create<LinkType::Duration, SerializationFormat::TUV>(const std::string& line, unsigned int id, double default_dur){
    assert(default_dur>0);
    std::istringstream iss(line);
    bool readed =false;
    timestamp t;
    nodeID u,v;
    readed = static_cast<bool>(iss >> t >> u >> v);
    t = t - default_dur/2;
    if (!(readed)) {
      std::cerr << "There is a problem with the line :" << std::endl << line
                << std::endl;
      exit(-1);
    }
    return tmpLink(id,t,u,v,default_dur);
}


template <>
inline tmpLink tmpLink::create<LinkType::Duration,SerializationFormat::TUVD>(const std::string& line, unsigned int id){
    std::istringstream iss(line);
    bool readed =false;
    timestamp t;
    nodeID u,v;
    long double duration;
    readed = static_cast<bool>(iss >> t >> u >> v >> duration);
    assert(duration>=0);
    if (!(readed)) {
      std::cerr << "There is a problem with the line :" << std::endl << line
                << std::endl;
      exit(-1);
    }
    return tmpLink(id,t,u,v,duration);
}
template <>
inline tmpLink tmpLink::create<LinkType::Duration,SerializationFormat::BEUV>(const std::string& line, unsigned int id){
    std::istringstream iss(line);
    bool readed =false;
    timestamp t_end;
    timestamp t;
    nodeID u,v;
    long double duration;
    readed = static_cast<bool>(iss >> t >> t_end >> u >> v);
    assert(t_end>=t);
    duration = t_end - t;
    if (!(readed)) {
      std::cerr << "There is a problem with the line :" << std::endl << line
                << std::endl;
      exit(-1);
    }
    return tmpLink(id,t,u,v,duration);
}

template <LinkType LT, SerializationFormat SF>
inline tmpLink tmpLink::create(const std::string& s, unsigned int id , double d){
    std::cerr << "create() not defined for "<< LT<<" "<< SF<<std::endl;
    std::cerr << "Parameter were "<< s<<" "<< id<< d <<std::endl;
    exit(-1);
}
template <LinkType LT, SerializationFormat SF>
inline tmpLink tmpLink::create(const std::string& s, unsigned int id){
    std::cerr << "create() not defined for "<< LT<<" "<< SF<<std::endl;
    std::cerr << "Parameter were "<< s<<" "<< id<< std::endl;
    exit(-1);
}


inline std::ostream &operator<<(std::ostream & out, const tmpLink & l){
    out<<l.t<<" "<<l.u<<" "<<l.v<<" "<<l.duration;
    return out;
}



//Assume that begin < end.
struct timespan {
    timestamp begin;
    timestamp end;
};


// compute the intersection between two intervals.
inline long double intersection(const timespan & s1,const timespan & s2){
    long double dif = std::min(s1.end, s2.end)- std::max(s1.begin, s1.begin);
    return dif;
}
class Node {
  template <LinkType LT> friend class linkStream;
  friend class TestMaster;

  nodeID ID;
  // TODO change to ref? or const
  std::deque<Link *> linkNeighbor;

  void add(Link &l);
  // Node(const Node &n) : ID(n.ID), linkNeighbor(n.linkNeighbor) {}
public:
  // Should be private
  Node(nodeID id_) : ID(id_) {}
  Node() : ID(0) {}


  nodeID id() const { return ID; }

  // return a deque of link connected to the node
  const std::deque<Link *> & linksNeighbor() const { return linkNeighbor; }

  // return the number of link connected to the node
  unsigned int degree() const { return linkNeighbor.size(); }

  void displayLinkNeighbor(std::string prefix = "") const;

  bool operator==(const Node &n) const { return ID == n.id(); }
  bool operator!=(const Node &n) const { return !(n == *this); }
  bool operator<(const Node &n) const { return ID < n.id(); }
};

std::ostream &operator<<(std::ostream &, const Node &);

struct nodeComparator {
  bool operator()(const Node *a, const Node *b) const {
    return a->id() < b->id();
  }
};

class Link {
  friend class TestMaster;
  template <LinkType LT>
  friend class linkStream;
  Node *leftNode;
  Node *rightNode;

  tmpLink makeTmpLink()const{
      return tmpLink(id,t,left().id(), right().id(), duration);
  }

  bool weakEqual(const Link &l) const{
      return id==l.id && std::fabs(t-l.t)<0.000001 && std::fabs(duration-l.duration)<0.000001 && left().id()==l.left().id() && right().id()==l.right().id();
  }
  bool weakEqual(const tmpLink &l) const{
      return id==l.id && std::fabs(t-l.t)<0.000001 && std::fabs(duration-l.duration)<0.000001 && left().id()==l.u && right().id()==l.v;
  }
public:
  //TODO should be private with a friend class.
  Link(tmpLink t, Node &leftN, Node &rightN)
      : leftNode(&leftN), rightNode(&rightN), id(t.id), t(t.t),
        duration(t.duration) {}
  //TODO should be private with a friend class.
  explicit Link(const Link &l)
      : leftNode(l.leftNode), rightNode(l.rightNode), id(l.id), t(l.t),
        duration(l.duration) {}
  const linkID id;
  const timestamp t;
  double duration;

  Node &left() const { return *leftNode; }

  Node &right() const { return *rightNode; }

  // True if the given nodeID is connected to the link.
  bool in(nodeID id) const {
    return (id == leftNode->id() || id == rightNode->id());
  }

  // Give the other node connected to the link
  const Node &otherNode(const Node &n) {
    if (!in(n.id())) {
      std::cerr << "The node " << n << " is not in the link " << this->leftNode
                << " " << this->rightNode << std::endl;
    }
    if (n != *leftNode) {
      return *leftNode;
    }
    return *rightNode;
  }

  // Give the degree of the link degre as the sum of the degree of each nodes (minus -2 because of the link)
  unsigned int sumDegree() const {
    return leftNode->degree() + rightNode->degree() - 2;
  }

  // Fill in the deque all the link adjacent to this
  // The deque also contains this.
  void linkNeighbor(std::deque<Link *> &) const;

  std::pair<bool, nodeID> commonNode(const Link &l) const;

  bool sameNodes(const Link &l) const {
    return (left().id() == l.left().id() || left().id() == l.right().id()) &&
           (right().id() == l.left().id() || right().id() == l.right().id());
  }

  bool before(const Link &l) const { return t < l.t; }
  bool after(const Link &l) const { return t > l.t; }

  double intersection(const Link &) const;
  double intersection(const tmpLink &) const;
  timespan lifeTime()const{return {t, t+duration};}
  timestamp ending()const{return t+duration;}
};
std::ostream &operator<<(std::ostream &, const Link &);

struct linkComparator {

  bool operator()(const Link *a, const Link *b) const { return a->id < b->id; }
};



template <LinkType LT>
class linkStream {

  friend class TestMaster;
  std::map<nodeID, Node> nodes;
  std::deque<Link> links;

  std::map<PairLink, std::deque<Link *>, PairLinkComp> events; // just a view of
                                                               // links in order
                                                               // to query : All
                                                               // timestamps at
                                                               // wich (a,b)
                                                               // appear.
  // Order in Pair Link matter.

  timestamp beginning, ending;
  unsigned int bufferSize;

  linkStream(const linkStream &); // prevent copy constructor
  const linkStream &
  operator=(const linkStream &l); // prevent assignation constructor

  Node &getCreateNode(nodeID nId) {
    std::map<nodeID, Node>::iterator it = nodes.find(nId);
    if (it == nodes.end()) {
      Node &n = nodes[nId]; // creation of the node
      n.ID = nId;
      return n;
    }
    return it->second;
  }

  std::pair <bool,std::deque<Link *> & >eventList(nodeID a, nodeID b);
  void addLink(const tmpLink &l);

  bool compLinkList(const linkStream &)const;
  bool compLinkList(const std::deque<tmpLink> &)const;

public:
  typedef std::deque<Link>::iterator linksIterator;
  typedef std::deque<Link>::const_iterator const_linksIterator;
  typedef std::map<nodeID, Node>::const_iterator const_NodesIterator;

  const_NodesIterator beginNodes() const { return nodes.begin(); }

  const_NodesIterator endNodes() const { return nodes.end(); }

  const_linksIterator beginLinks() const { return links.begin(); }

  const_linksIterator endLinks() const { return links.end(); }

  linksIterator beginLinks() { return links.begin(); }

  linksIterator endLinks() { return links.end(); }

  std::map<PairLink, std::deque<Link *>, PairLinkComp>::const_iterator beginEvent()const{return events.begin();}
  std::map<PairLink, std::deque<Link *>, PairLinkComp>::const_iterator endEvent()const{return events.end();}

  explicit linkStream(unsigned int=MAX_UINT);

  template<SerializationFormat SF=defaultValue(LT)>
  void readFile(const char *, double d_duratiosn);

  template<SerializationFormat SF=defaultValue(LT)>
  void readFile(const char *);

  template<SerializationFormat SF=defaultValue(LT)>
  bool readBuffer(std::ifstream &);


  const Link &link(linkID lId) const {
    if(static_cast<unsigned int >(lId) >= links.size()){
      std::cerr << "Wrong linkId" << std::endl;
      throw std::string("Wrong linkId");
    }
    return links[lId];
    }

  const Node &node(nodeID nId) const {
    std::map<nodeID, Node>::const_iterator it = nodes.find(nId);
    if (it == nodes.end()) {
      std::cerr << "Node: wrong NodeId" << std::endl;
      throw std::string("Node: wrong NodeId");
    }
    return it->second;
  }

  void printDegreeList(std::ostream &f = std::cout) const;

  unsigned int getNbLinks() const { return links.size(); }

  void dump() const {
    const_linksIterator it = beginLinks(), ite = endLinks();
    for (; it != ite; ++it) {
      const Link &l(*it);
      std::cout << "\t" << l << std::endl;
    }
  }

  // return all the links between a and b. Order matter.
  std::pair <bool,const std::deque<Link *> & >eventList(nodeID a, nodeID b) const;

  // Return true if the linkstream is simple.
  bool isSimple()const;

  linkStream<LT> makeSimple()const;

  timestamp beginLife()const{return beginning;}
  timestamp endLife()const{return ending;}

  unsigned int getNbNodes() const { return nodes.size(); }
};
#include "linkStream.tpp"
} // namespace end
#endif
