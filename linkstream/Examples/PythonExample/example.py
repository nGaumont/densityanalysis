import PyLinkstream as LS


L = LS.Durationstream()
L.readFile(LS.SerializationFormat.TUVD, "../LinkStreamExamples/test1.txt")

# iterate over the nodes
print("Nodes:")
for node in L.nodes():
    print(node)
print()

print("Links:")
# iterate over the links
for link in L.links():
    print(link)
print()

print("Activation of (1,2)")
#iterate over all the link between 1 and 2
for link in L.activations(L.node(1), L.node(2)):
    print(link)
print()

print("Edges:")
# iterate over the existing edges
for edge in L.edges():
    print("link :", edge.nodes())
    # iterate over all the links between nodes();
    for activation in edge:
        print("\t", activation)
print()


# read the communtiy structure
C = LS.Cover("../LinkStreamExamples/aff_test1.txt",L)
print("Groups:")
for group in C.groups():
    D = LS.DensityAnalyser( group, True)
    dens= D.between(group.lifeTime().start(),group.lifeTime().end())
    print("\t Density of",group.id(),"=",dens)

print()
