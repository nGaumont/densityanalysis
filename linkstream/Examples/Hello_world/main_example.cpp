#include <iostream>
#include <string>
#include "../../include/linkStream.hpp"
#include "../../include/community.hpp"
#include "../../include/densityAnalyser.hpp"

using namespace gaumont;

static void show_usage(std::string name) {
  std::cerr << "This an example prgm: " << std::endl;
  std::cerr << "Usage: " << name << " -i temporalNet -c ComFile"
            << std::endl
            << "Options:\n"
            << "\t-h,--help\t\tShow this help message\n"
            << "\t-i,--input input\t\t file to read 't u v d'\n"
            << "\t-c ComFile\t\t affiliation list of linkID comID\n"
            << std::endl;
  exit(-1);
}

struct Argument {
  char *inputFile;
  char *comFile;
};

static Argument parse_args(int argc, char **argv) {
  Argument res;
  res.inputFile = NULL;
  res.comFile = NULL;
  if (argc < 2) {
    show_usage(argv[0]);
    exit(-1);
  }

  for (int i = 1; i < argc; ++i) {
    std::string arg = argv[i];
    if ((arg == "-h") || (arg == "--help")) {
      show_usage(argv[0]);
    } else if ((arg == "-i") || (arg == "--input")) {
      if (i + 1 < argc) {
        res.inputFile = argv[++i];
        std::cout << "temporalNet = " << res.inputFile << std::endl;
      } else {
        std::cerr << "--input option requires one argument." << std::endl;
        exit(-1);
      }
  } else if (arg == "-c") {
      if (i + 1 < argc) {
        res.comFile = argv[++i];
        std::cout << "ComFile = " << res.comFile << std::endl;
      } else {
        std::cerr << "-c option requires one argument." << std::endl;
        exit(-1);
      }
    }  else {
      show_usage(argv[0]);
    }
  }
  if (res.inputFile == NULL || res.comFile == NULL) {
    show_usage(argv[0]);
    exit(-1);
  }
  return res;
}

int main(int argc, char **argv) {
  Argument arg = parse_args(argc, argv);
  linkStream<Duration> L;
  L.readFile(arg.inputFile);

  long double count=0;
  for(linkStream<Simple>::const_linksIterator it = L.beginLinks(); it != L.endLinks();++it){
      count += it->duration;
  }
  std::cout<<"Somme des durées = "<<count<<std::endl;
  Cover C(arg.comFile, L);
  if(!C.checkCoverage(L)){
    std::cout << "La partition ne recouvre pas tout les liens" << std::endl;
    exit(-1);
  }else{
    std::cout << "La partition recouvre tout les liens" << std::endl;
  }
  long double mean_size=0;
  for(const Group & g : C){
      mean_size += g.inducedNode().size();
  }
  mean_size = mean_size/C.size();
  std::cout<<"Chaque groupe a en moyenne "<<mean_size<<" noeuds."<<std::endl;
  std::set<nodeID> nodes;

  for(auto it = L.begin(); it !=L.end();++it){
      nodes.insert(it->first);
  }
  DensityAnalyser d(L,nodes);
  d.variableDuration(2.5, 42,-3);
  return 0;
}
