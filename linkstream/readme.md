linkstream
==========
This provides everything to manipulate a linkstream in C++ in a shared library.
The first class is linkStream and can read and manipulate a linkstream.  
The other ones are Cover and Group which can manipulate groups of links (only as a partition).  

# Installation

To build the library, type:  
```
cd build
cmake ..
make
```  

To make sure everything works  
```
ctest
```  
## Use the lib in custom C++ project
In order to include the lib in your project, don't forget to include the header file which are in the ```include``` folder and use the followings commands to compile
```
g++ -LPATH/TO/lib -Wl,-rpath,PATH/TO/lib -lLinkstream -o PROJECT_NAME PROJECT_SOURCE.cpp -O9 -std=c++11 -Wall
```
See the makefile rules for the example to update your makefile.  
You may also use cmake which make it everything easyier.


# Examples  

There are examples in the Examples folder.
Examples are either in C++ or in python  
To run C++ example:
```
cd  Path/Example
make
./EXEC
```

To run python example:  
```
cd  Path/Example
python FILE.py
```

## Dependancies
GCC has to support c++11 in order to compile the project.
